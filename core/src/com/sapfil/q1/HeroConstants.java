package com.sapfil.q1;

/**
 * Created by Sapfil on 10.09.2016.
 */
public interface HeroConstants {

    public static final float   HERO_CRASH_FREEZE = 0.5f;   // losing control time after collision
    public static final float     HERO_MAX_SPEED = 10.0f;       // units (meters) in sec
    public static final float     HERO_MAX_SPEED2 = HERO_MAX_SPEED * HERO_MAX_SPEED;       // pixel in sec
    public static final float   HERO_MAX_ROTATION_SPEED = 3.0f; // pixel in sec
    public static final float   HERO_RADIAL_ACCELERATION = 0.7f; // grad in sec

    public static final float   HERO_ACCELERATION = 1.5f;   // abs acceleration
    public static final float   HERO_NORMAL_ACCELERATION = 0.5f;   // abs acceleration
    public static final int     HERO_BRAKING = 2;       // relative speed afted 1 scond

    public static final float   EMPTY_POINT_STOP_DISTANCE = 1.5f; //distanceto point when braking starts
    public static final float   DRILL_MAX_DISTANCE = 2.0f            ;
    public static final float   MIN_STAY_AND_MOVE_SPEED = 0.5f;            // hero speed nearly "stand still" when STAY_AND_AIM-Status goes to STAY-Status.
    public static final float   MIN_STAY_AND_MOVE_SPEED2 = MIN_STAY_AND_MOVE_SPEED * MIN_STAY_AND_MOVE_SPEED;  // square for Vector.len2.
    public static final float   AIM_ANGLE_COS   = 0.5f;         // hero will turn around withoun moving until angle(cos) to aim is less this float
    public static final float   AIM_ANGLE_SIN_MAX_ERROR = 0.2f; // hero may fly not directly to point. Direct fly without error will twitch sprite in every frame
    public static final float   AIM_ANGLE_SIN_MAX_ON_THE_FLY = 0.4f; // hero may fly not directly to point. Direct fly without error will twitch sprite in every frame
}
