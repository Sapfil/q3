package com.sapfil.q1;

import com.sapfil.q1.UI.UI;

import static com.sapfil.q1.UI.UI.DebugMode.OFF;

/************************************************
 * Created by Sapfil on 13.01.2016.
 * Last edit: 02-08-2016
 * TODO delete not-used constants
 * Notes:
 ***********************************************/
public interface GlobalConstants {


// ===========================================================
// NEW FIELDS
// ===========================================================

    // main

    public static float GFX_TO_PHYS = 0.1f;

    int CELL_PIXEL_SIZE_X = 200;
    int CELL_PIXEL_SIZE_Y = 200;

    float CELL_PHYS_SIZE_X = CELL_PIXEL_SIZE_X * GFX_TO_PHYS;
    float CELL_PHYS_SIZE_Y = CELL_PIXEL_SIZE_Y * GFX_TO_PHYS;

    // debug

    int CELL_TECH_SPRITE_SIZE = 128;

// ===========================================================
// OLD FIELDS
// ===========================================================


    enum QUAD_Direction {UP, UP_LEFT, LEFT, DOWN_LEFT, DOWN, DOWN_RIGHT, RIGHT, UP_RIGHT, CENTER};

    public static float ON_SCREEN_BIAS = 1.1f;

    public static final int WIDTH = 800;
    public static final int HEIGHT = 480;

    UI.DebugMode DEBUG_MODE = OFF;
    UI.DebugMode DEBUG_CELL_TECH_SPRITE = DEBUG_MODE;

    boolean CREATE_NEW_WORLD_ON_START = false;
    boolean CREATE_HEX_WORLD_ON_START = true;
    int START_ASTEROIDS_COUNT =  50;
    int START_WORLD_SIZE = 1000; // pixels to left and 1000 to right, ud, down from start point (0, 0)

    byte WORLD_GRID_STEP_X = 45;
    byte WORLD_GRID_STEP_Y = 26;
    byte CELL_HEX_SIZE_X = 12;
    byte CELL_HEX_SIZE_Y = 6;


//===========================================================
// Methods
// ===========================================================
}

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

