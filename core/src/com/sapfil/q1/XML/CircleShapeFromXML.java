package com.sapfil.q1.XML;

import com.badlogic.gdx.math.Vector3;

/********************************
 * Created by Sapfil 
 * Edit 21.05.2017.
 * Notes
 ********************************/

public class CircleShapeFromXML implements ShapeDefFromXML {

    private Vector3 coordsXYR = new Vector3();

    public CircleShapeFromXML(int centerX, int centerY, int radius) {
        coordsXYR.add(centerX, centerY, radius);
    }

    @Override
    public Type getType() {
        return Type.SPHERE;
    }

    @Override
    public Object getData() {
        return coordsXYR;
    }
}
