package com.sapfil.q1.XML;

/********************************
 * Created by Sapfil 
 * Edit 21.05.2017.
 * Notes
 ********************************/

public class PolygonShapeFromXML implements ShapeDefFromXML {

    float[] vertexes;

    public PolygonShapeFromXML(float[] vertexes) {
        this.vertexes = vertexes;
    }

    @Override
    public Type getType() {
        return Type.POLYGON;
    }

    @Override
    public Object getData() {
        return vertexes;
    }
}
