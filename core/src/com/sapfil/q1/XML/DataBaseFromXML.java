package com.sapfil.q1.XML;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/************************************************
 * Created by Sapfil on ... 01.2016.
 * Last edit: 02-10-2016
 * Notes: Mess-cleaning
 ***********************************************/

public class DataBaseFromXML extends XmlReader
{

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private Texture currentTexture;
    public final HashMap<Integer, BasicPartSample> basicPartMap = new HashMap<Integer, BasicPartSample>(); // contains all GFX-data
    public final HashMap<Integer, HealthPartSample> healthPartMap = new HashMap<Integer, HealthPartSample>(); // contains physics data (health, body size and form, type of blas when die
    public final HashMap<Integer, ShapeDefFromXML> bodiesMap = new HashMap<Integer, ShapeDefFromXML>(); // contains shapesDefinition
    public final HashMap<Integer, List<DependancySample>> dependMap = new HashMap<>();  // contains

    // ===========================================================
    // Constructors
    // ===========================================================

    public DataBaseFromXML()	throws Exception {

        FileHandle inputFile = Gdx.files.internal("dataBase.xml");
        //FileHandle inputFile = Gdx.files.internal("XML/dataBase.xml");
        if (!inputFile.file().exists())
            Gdx.app.log("Sapfil_Log", "ERROR in loading DataBase.");
        Element root = this.parse(inputFile);

        for (int i = 0; i < root.getChildCount(); i++) {
            String chapterName = root.getChild(i).getName();
            if (!chapterName.equals("bodies")) {
                currentTexture = new Texture(chapterName + ".png");
                for (int j = 0; j < root.getChild(i).getChildCount(); j++) {

                    int id = root.getChild(i).getChild(j).getIntAttribute("id", 0);
                    int too = root.getChild(i).getChild(j).getIntAttribute("too", 0);
                    int texture_x = root.getChild(i).getChild(j).getIntAttribute("x", 0);
                    int texture_y = root.getChild(i).getChild(j).getIntAttribute("y", 0);
                    int width = root.getChild(i).getChild(j).getIntAttribute("w", 0);
                    int height = root.getChild(i).getChild(j).getIntAttribute("h", 0);
                    int fps = root.getChild(i).getChild(j).getIntAttribute("fps", 0);
                    int framesInRow = root.getChild(i).getChild(j).getIntAttribute("fw", 1);
                    int framesInColomn = root.getChild(i).getChild(j).getIntAttribute("fh", 1);
                    int origin_x = root.getChild(i).getChild(j).getIntAttribute("ox", width / 2 / framesInRow);
                    int origin_y = root.getChild(i).getChild(j).getIntAttribute("oy", height / 2 / framesInColomn);
                    int states = root.getChild(i).getChild(j).getIntAttribute("states", 1);

                    basicPartMap.put(id, new BasicPartSample(id, too,
                            fps, framesInRow, framesInColomn,
                            texture_x, texture_y, width, height, origin_x, origin_y, states, currentTexture));

                    // health sample creates only if object have health != 0
                    int health = root.getChild(i).getChild(j).getIntAttribute("health", 0);
                    if (health != 0) {
//                        int bodyType = root.getChild(i).getChild(j).getIntAttribute("bt", 0);
//                        int body_width = root.getChild(i).getChild(j).getIntAttribute("bw", 0);
//                        int body_height = root.getChild(i).getChild(j).getIntAttribute("bt", 0);
                        int tob = root.getChild(i).getChild(j).getIntAttribute("tob", 0);
                        int type = root.getChild(i).getChild(j).getIntAttribute("type", 0);
//                        int body_x = root.getChild(i).getChild(j).getIntAttribute("bx", 0);
//                        int body_y = root.getChild(i).getChild(j).getIntAttribute("by", 0);
                        int[] bodies = new int[root.getChild(i).getChild(j).getIntAttribute("bodies", 0)];
                        for (int k = 0; k < bodies.length; k++)
                            bodies[k] = root.getChild(i).getChild(j).getIntAttribute("b"+Integer.toString(k), 0);

                        healthPartMap.put( id, new HealthPartSample(id, health, tob,
                                (type == 0 ? BodyDef.BodyType.StaticBody :
                                 type == 1 ? BodyDef.BodyType.DynamicBody : BodyDef.BodyType.KinematicBody)
                                , bodies));
                    }

                    int childId = root.getChild(i).getChild(j).getIntAttribute("id1", 0);
                    if (childId > 0){
                        int childCount = 1;
                        List<DependancySample> dependancySamplesList = new LinkedList<>();
                        while (root.getChild(i).getChild(j).getIntAttribute("id"+String.valueOf(childCount), 0) > 0){
                            dependancySamplesList.add(new DependancySample(
                                            root.getChild(i).getChild(j).getIntAttribute("id"+String.valueOf(childCount)),
                                            root.getChild(i).getChild(j).getIntAttribute("x"+String.valueOf(childCount)),
                                            root.getChild(i).getChild(j).getIntAttribute("y"+String.valueOf(childCount))
                                ));
                            childCount++;
                        }
//                        int[] childrenArray = new int[childCount];
//                        for (int c = 0; c < childCount - 1; c++ )
//                            childrenArray[c] = root.getChild(i).getChild(j).getIntAttribute("id"+String.valueOf(childCount + 1 ));
                        dependMap.put(id , dependancySamplesList);
                    }
                }
            }
            // bodies
            else
                    for (int j = 0; j < root.getChild(i).getChildCount(); j++){
                        int bodyID = root.getChild(i).getChild(j).getIntAttribute("bid", 0);
                        int vertexCount = root.getChild(i).getChild(j).getIntAttribute("count", -1);
                        if (vertexCount == 0)
                            bodiesMap.put(bodyID, new CircleShapeFromXML(
                                root.getChild(i).getChild(j).getIntAttribute("x", 0),
                                root.getChild(i).getChild(j).getIntAttribute("y", 0),
                                root.getChild(i).getChild(j).getIntAttribute("r", 0)
                            ));
                        else if (vertexCount > 0){
                            float[] vertexes = new float[vertexCount*2];
                            for (int k = 0; k < vertexCount; k++){
                                vertexes[k*2]   = root.getChild(i).getChild(j).getIntAttribute("x"+Integer.toString(k), 0);
                                vertexes[k*2+1] = root.getChild(i).getChild(j).getIntAttribute("y"+Integer.toString(k), 0);
                            }
//                            for (int k = 0; k < vertexCount; k++){
//                                vertexList.add(new Vector2(
//                                        root.getChild(i).getChild(j).getIntAttribute("x"+Integer.toString(k), 0),
//                                        root.getChild(i).getChild(j).getIntAttribute("y"+Integer.toString(k), 0)
//                                ));
//                            }
                            bodiesMap.put(bodyID, new PolygonShapeFromXML(vertexes));
                        }
                }
        }
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================
    // ===========================================================
    // Methods
    // ===========================================================
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    // my class for saving object-samples-information
    public class BasicPartSample{
        public int id, type_of_object, origin_x, origin_y;
        public TextureRegion[] textureRegion;
        public int fps, states;
        public float duration = 0;


        private BasicPartSample (int _id, int _too,
                         int _fps, int _framesInRow,  int _framesInColomn,
                         int _texture_x, int _texture_y, int _width, int _height, int origin_x, int origin_y,
                                 int states,
                                 Texture _currentTexture)	{

//            if (_id == 35 )
//                System.out.println();

            this.type_of_object = _too;
            this.id = _id;
            this.origin_x = origin_x; this.origin_y = origin_y;
            this.fps = _fps;
            if (fps !=0)
                duration = 1/(float)fps;
            if (_width > 0 && _height > 0) {
                TextureRegion tmpTextureRegion = new TextureRegion(_currentTexture, _texture_x, _texture_y, _width, _height);
                TextureRegion[][] tmpRegionsMassive = tmpTextureRegion.split((int) (_width / _framesInRow), (int) (_height / _framesInColomn));
                textureRegion = new TextureRegion[_framesInRow * _framesInColomn];

                for (int i = 0; i < _framesInRow; i++)
                    for (int j = 0; j < _framesInColomn; j++)
                        textureRegion[i + j * _framesInRow] = tmpRegionsMassive[j][i];
            }
        }
    }

    public class HealthPartSample {

        public int id;
        public int health;
        public int type_of_blast;
        public int[] bodies;
        public BodyDef.BodyType type;
        HealthPartSample(int id, int health, int tob, BodyDef.BodyType type, int[] bodies){
            this.id             = id;
            this.type_of_blast  = tob;
            this.health         = health;
            this.bodies         = bodies;
            this.type           = type;
        }
    }

    public class DependancySample{

        public int dependantID;
        public int x;
        public int y;

        public DependancySample(int dependantID, int x, int y) {
            this.dependantID = dependantID;
            this.x = x;
            this.y = y;
        }
    }
}


