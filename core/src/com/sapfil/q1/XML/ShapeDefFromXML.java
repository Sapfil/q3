package com.sapfil.q1.XML;

/********************************
 * Created by Sapfil 
 * Edit 21.05.2017.
 * Notes
 ********************************/

public interface ShapeDefFromXML {

    public enum Type{UNDEFINED, SPHERE, POLYGON}

    public Type getType();

    public Object getData();

}
