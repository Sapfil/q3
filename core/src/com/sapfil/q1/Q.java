package com.sapfil.q1;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.sapfil.q1.object.GFXObjects.AGFXO;
import com.sapfil.q1.object.GFXObjects.ZSortedGFXCollecion;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.object.GameObjects.GameObjectsPool;
import com.sapfil.q1.object.PhysicsObjects.HealthObject;
import com.sapfil.q1.state.GameStateManager;
import com.sapfil.q1.XML.DataBaseFromXML;

public class Q extends ApplicationAdapter {


	// ===========================================================
	// Constants and static fields
	// ===========================================================

	public static final String TITLE = "Q demo 1";

	// ===========================================================
	// Fields
	// ===========================================================

	private GameStateManager mGSM;
	private SpriteBatch mBatch;
	private DataBaseFromXML mDB;

	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces (U/R/D)
	// ===========================================================

	@Override
	public void create () {

		try { mDB = new DataBaseFromXML(); }  catch (Exception e) { e.printStackTrace();		}

		AGFXO.sDB 		= mDB;
		HealthObject.sDB 			= mDB;
		ZSortedGFXCollecion.sDB		= mDB;
		GameObject.sDB				= mDB;

		GameObject.pool = new GameObjectsPool();

		Box2D.init();

		mGSM = new GameStateManager();
		mBatch = new SpriteBatch();
		Gdx.gl.glClearColor(0, 0, 0.5f, 1);
	}

	@Override
	public void render () {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		mGSM.update(Gdx.graphics.getDeltaTime());
		mGSM.render(mBatch);

	}

	@Override
	public void dispose () {
		mBatch.dispose();
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
