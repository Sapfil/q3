package com.sapfil.q1.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.layer.LayerImpl;
import com.sapfil.q1.layer.LayerType;
import com.sapfil.q1.object.GFXObjects.AGFXO;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.UI.UI;
import com.sapfil.q1.UI.UserCommand;
import com.sapfil.q1.UI.UserCommandResolver;
import com.sapfil.q1.UI.UserCommandType;

/************************************************
 * Created by Sapfil on 16.09.2016.
 * Last edit: 02-10-2016 sBlasts added
 * Notes: Memory-loss check
 ***********************************************/
public class EditorState extends State {

    public enum MODE{
        MOVE,
        ADD,
        DELETE
    }

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static UI ui;

    // ===========================================================
    // Fields
    // ===========================================================

    private BitmapFont techInfoFont;

    private GameObject mHero;

    private MODE mode = MODE.ADD;
    private Object modeInfo = 103;

//    PerformanceCounter perfGrid, perfUI,  perfHero;

    // ===========================================================
    // Constructors
    // ===========================================================

    public EditorState(GameStateManager pGSM) {
        super(pGSM);

        addHero();

        ui = new UI(layerMap.get(LayerType.UI));
        addEditorButtons();

        ui.transponCoords(0, 0);

        techInfoFont = new BitmapFont();
        techInfoFont.getData().scaleX = 0.1f;
        techInfoFont.getData().scaleY = 0.1f;
        techInfoFont.setUseIntegerPositions(false);

        focusSet(mHero.getAbsX(), mHero.getAbsY());
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    @Override
    protected void inputHandler() {

        mouse.x = Gdx.input.getX();
        mouse.y = Gdx.input.getY();
        if (Gdx.input.justTouched()) {
            UserCommand userCommand = ui.resolveTouch(mouse.x, mouse.y);
            if (userCommand != null) {
                switch (userCommand.type) {
                    case B_DEBUG_MOVE_HERO: {
                        mode = MODE.MOVE;
                        break;
                    }
                    case B_DEBUG_DELETE_OBJECTS: {
                        mode = MODE.DELETE;
                        break;
                    }
                    case B_DEBUG_ADD_OBJECTS: {
                        mode = MODE.ADD;
                        modeInfo = userCommand.data;
                        break;
                    }
                    case CHANGE_RENDER_TYPE: {
                        ui.changeDebugMode();
                        break;
                    }
                    case B_DEBUG_SAVE_WORLD: {
                        break;
                    }
                }
                return;
            }
        }
        UserCommand command = UserCommandResolver.resolveEditorTouch(mouse, camera, layerMap.get(LayerType.MAIN).getHealthObjectCollection(), !mode.equals(MODE.MOVE));
        if (command != null)
        switch (command.type){

            case FREE_TOUCH:{
                switch (mode){
                    case ADD:{
//                        worldGrid.addObject(
//                                MathUtils.round(((Vector3)command.data).x),
//                                MathUtils.round(((Vector3)command.data).y),
//                                (int)modeInfo);
                        break;
                    }
                    case MOVE:{
                        mHero.getLocalCoordinates().setX(((Vector3)command.data).x * GlobalConstants.WORLD_GRID_STEP_X * GlobalConstants.GFX_TO_PHYS);
                        if (((Vector3)command.data).x%2 == 0)
                            mHero.getLocalCoordinates().setY(((Vector3)command.data).y * GlobalConstants.WORLD_GRID_STEP_Y * 2 * GlobalConstants.GFX_TO_PHYS);
                        else
                            mHero.getLocalCoordinates().setY((((Vector3)command.data).y * GlobalConstants.WORLD_GRID_STEP_Y * 2 + GlobalConstants.WORLD_GRID_STEP_Y)
                                    * GlobalConstants.GFX_TO_PHYS);
                        ui.transponCoords(mHero.getLocalCoordinates().getX(),
                                            mHero.getLocalCoordinates().getY());
                        break;
                    }
                    case DELETE:{
//                        worldGrid.deleteObjects(
//                                MathUtils.round(((Vector3)command.data).x),
//                                MathUtils.round(((Vector3)command.data).y));
                        break;
                    }
                    default:{}
                }
                break;
            }
            default: {} //do nothing
        }
    }

    @Override
    public void update(float dt) {

        //TODO move to state
        //TODO create half-consts
        //TODO add bias to borders
        focusSet(mHero.getAbsX(), mHero.getAbsY());

        AGFXO.screenLeftBorder = mHero.getAbsX() - GlobalConstants.WIDTH/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;
        AGFXO.screenRightBorder = mHero.getAbsX() + GlobalConstants.WIDTH/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;
        AGFXO.screenDownBorder = mHero.getAbsY() - GlobalConstants.HEIGHT/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;
        AGFXO.screenUpBorder = mHero.getAbsY() + GlobalConstants.HEIGHT/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;

        ui.update(dt);
    }


    @Override
    public void render(SpriteBatch pSB) {
        super.render(pSB);
        // *** tech-info
        techInfoRender(pSB);
        return;
    }

    @Override
    public void dispose() {
        super.dispose();
        techInfoFont.dispose();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public Vector2 getCenterCoords(){
        return new Vector2(mHero.getAbsX(), mHero.getAbsY());
    }

    private void addHero() {
            mHero = new GameObject.GameObjectBuilder()
                    .create(9, layerMap.get(LayerType.MAIN))
                    .withZcoord(1000)
                    .withXYcoords(0, 0)
                    .build();
    }

    private void techInfoRender(SpriteBatch pSB){
        pSB.begin();
        techInfoFont.draw(pSB, "FPS:       " + Integer.toString(Gdx.graphics.getFramesPerSecond()), currentFocus.x - (GlobalConstants.WIDTH / 2)/10 + 5.2f, currentFocus.y + (GlobalConstants.HEIGHT / 2 - 40)/10);

        layerMap.get(LayerType.MAIN).physicsDebugRender(camera);

        pSB.end();
    }

    private void addEditorButtons() {
        ui.addButton(60006, new Vector2(-375, 165), new UserCommand(UserCommandType.B_DEBUG_SAVE_WORLD));
        ui.addButton(60100, new Vector2(375, 215), new UserCommand(UserCommandType.B_DEBUG_MOVE_HERO));
        ui.addButton(60101, new Vector2(375, 165), new UserCommand(UserCommandType.B_DEBUG_DELETE_OBJECTS));
        ui.addButton(new int[]{103, 104, 105}, new Vector2(375, 115), new UserCommand(UserCommandType.B_DEBUG_ADD_OBJECTS));
        ui.addButton(new int[]{60003, 60002, 60005}, new Vector2(-375, 215), new UserCommand(UserCommandType.CHANGE_RENDER_TYPE));
    }



    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
