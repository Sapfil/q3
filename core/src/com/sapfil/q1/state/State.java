package com.sapfil.q1.state;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.layer.LayerImpl;
import com.sapfil.q1.layer.LayerType;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import platform.Disposable;

import static com.sapfil.q1.GlobalConstants.GFX_TO_PHYS;

/**
 * Created by Sapfil on 16.09.2016.
 */
public abstract class State implements Disposable {

    protected OrthographicCamera camera;
    protected Vector3 mouse;
    protected GameStateManager gsm;
    protected Map<LayerType, Layer> layerMap = new HashMap<>();

    protected Vector3 currentFocus = new Vector3();
//    protected ArrayList<LayerImpl> layerMap = new ArrayList<>();

    protected void focusSet(Vector3 v3)
    {
        camera.translate(v3.x - currentFocus.x, v3.y - currentFocus.y);
        currentFocus.x = v3.x;
        currentFocus.y = v3.y;
    }

    protected void focusSet(float pX, float pY)
    {
        camera.translate(pX  - currentFocus.x, pY - currentFocus.y);
        currentFocus.x = pX;
        currentFocus.y = pY;
    }

    protected void focusTranslate(float pX, float pY){
        camera.translate(pX, pY);
        currentFocus.x = pX;
        currentFocus.y = pY;
    }

    public State(GameStateManager pGSM){
        this.gsm = pGSM;
        this.camera = new OrthographicCamera();
        camera.setToOrtho(false, GlobalConstants.WIDTH * GFX_TO_PHYS , GlobalConstants.HEIGHT * GFX_TO_PHYS );
        camera.translate(-GlobalConstants.WIDTH/2 * GFX_TO_PHYS, -GlobalConstants.HEIGHT/2 * GFX_TO_PHYS);
        this.mouse = new Vector3();
        layerMap.put(LayerType.BACK, new LayerImpl("back"));
        layerMap.put(LayerType.MAIN, new LayerImpl("main"));
        layerMap.put(LayerType.DEBUG, new LayerImpl("debug"));
        layerMap.put(LayerType.UI, new LayerImpl("ui"));
    }

    protected abstract void inputHandler();
    public void update (float dt){

        for (Layer layer : layerMap.values()){
            layer.update(dt);
        }
    }

    public void render(SpriteBatch pSB){

        camera.update();
        pSB.setProjectionMatrix(camera.combined);
        layerMap.get(LayerType.BACK).render(pSB);
        layerMap.get(LayerType.MAIN).render(pSB);
        layerMap.get(LayerType.DEBUG).render(pSB);
        layerMap.get(LayerType.UI).render(pSB);

    }

    public void dispose(){
        for (Layer layer : layerMap.values()){
            layer.dispose();
        }
    }

    public Supplier<Vector3> focusSupplier = () -> currentFocus;
    public Supplier<Float> widthSupplier = () -> GlobalConstants.WIDTH * camera.zoom * GFX_TO_PHYS ;
    public Supplier<Float> heightSupplier = () -> GlobalConstants.HEIGHT * camera.zoom * GFX_TO_PHYS;

}
