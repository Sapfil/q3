package com.sapfil.q1.state;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/************************************************
 * Created by Sapfil on 16.09.2016.
 * Last edit: 02-10-2016
 * Notes: Memory-loss-check
 ***********************************************/
public class GameStateManager {

    public enum stateType {PLAY, BASE}

    private Stack<com.sapfil.q1.state.State> States;
    private com.sapfil.q1.state.State playState = new PlayState(this);
    private com.sapfil.q1.state.State baseState = new BaseState(this);

    public GameStateManager(com.sapfil.q1.state.State state) {
        States = new Stack<>();
        this.push(new EditorState(this));
    }

    public GameStateManager(){
        States = new Stack<>();
        this.push(new StartState(this));
    }

    public void push (com.sapfil.q1.state.State pState){
        States.push(pState);
    }

    public void pop(){
        States.pop();
    }

    public com.sapfil.q1.state.State peek(){
        return States.peek();
    }

    public void  set(State pState){
        this.pop();
        this.push(pState);
    }

    public void set(stateType type){
        switch (type) {
            case BASE: {this.set(new com.sapfil.q1.state.StateValve(this, this.peek(), baseState)); break;}
            case PLAY: {this.set(new StateValve(this, this.peek(), playState)); break;}
        }
    }

    public void update (float dt){
        States.peek().update(dt);
        States.peek().inputHandler();
    }

    public void render(SpriteBatch pSB){
        States.peek().render(pSB);
    }

    public void dispose() {
        while (!States.empty()){
            States.peek().dispose();
            States.removeElement(States.peek());
        }
    }
}
