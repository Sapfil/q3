package com.sapfil.q1.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.layer.LayerType;
import com.sapfil.q1.object.GFXObjects.AGFXO;
import com.sapfil.q1.object.GameObjects.GameObject;

/********************************
 * Created by Sapfil 
 * Edit 12.06.2017.
 * Notes
 ********************************/

public class StartState extends State {

    private GameObject mBaseBackTexture;


    public StartState(com.sapfil.q1.state.GameStateManager pGSM/*, PlayState playState*/) {
        super(pGSM);
            mBaseBackTexture = new GameObject.GameObjectBuilder()
                    .create(63200, layerMap.get(LayerType.MAIN))
                    .withZcoord(-10000)
                    .build();


        AGFXO.screenLeftBorder =  - GlobalConstants.WIDTH/2;
        AGFXO.screenRightBorder =  GlobalConstants.WIDTH/2;
        AGFXO.screenDownBorder = - GlobalConstants.HEIGHT/2;
        AGFXO.screenUpBorder = GlobalConstants.HEIGHT/2;
    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.justTouched()){
            gsm.set(GameStateManager.stateType.PLAY);
        }
    }

    @Override
    public void render(SpriteBatch pSB) {
        super.render(pSB);
    }

    @Override
    public void dispose() {
        mBaseBackTexture.dispose(false);
    }
}
