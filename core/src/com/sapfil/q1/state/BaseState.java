package com.sapfil.q1.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.q1.layer.LayerType;
import com.sapfil.q1.object.GameObjects.GameObject;

/********************************
 * Created by Sapfil 
 * Edit 12.06.2017.
 * Notes
 ********************************/

public class BaseState extends State {

//    private PlayState playState;

    private GameObject mBaseBackTexture;


    public BaseState(GameStateManager pGSM/*, PlayState playState*/) {
        super(pGSM);
            mBaseBackTexture = new GameObject.GameObjectBuilder()
                    .create(63100, layerMap.get(LayerType.MAIN))
                    .withZcoord(-10000)
                    .build();

    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.justTouched()){
//            gsm.set(new StateValve(gsm, this, playState));
            gsm.set(GameStateManager.stateType.PLAY);
        }
    }

    @Override
    public void render(SpriteBatch pSB) {

        super.render(pSB);
    }

    @Override
    public void dispose() {
        mBaseBackTexture.dispose(false);
    }
}
