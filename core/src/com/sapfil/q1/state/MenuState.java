package com.sapfil.q1.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/************************************************
 * Created by Sapfil on 16.09.2016.
 * Last edit: 02-10-2016
 * Notes:   Memory-loss check
 ***********************************************/
public class MenuState extends State {

    private Texture mBackTexture, mButtonTexture;


    public MenuState(GameStateManager pGSM) {
        super(pGSM);

//        mBackTexture    = new Texture("startScreen.png");
//        mButtonTexture  = new Texture("Button.png");

    }

    @Override
    protected void inputHandler() {
        if (Gdx.input.justTouched()){
            gsm.set(new PlayState(gsm));
        }
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch pSB) {

//        camera.updateCellsList();
//        pSB.setProjectionMatrix(camera.combined);
//
//        pSB.begin();
//
//        pSB.draw(mBackTexture, -Q.WIDTH/2, -Q.HEIGHT/2, Q.WIDTH, Q.HEIGHT);
//        pSB.draw(mButtonTexture, DisturbingSpace.WIDTH/2 - mButtonTexture.getWidth()/2 - 400, 130 - 240);
//
//        pSB.end();

    }

    @Override
    public void dispose() {
//        mButtonTexture.dispose();
//        mBackTexture.dispose();
    }
}
