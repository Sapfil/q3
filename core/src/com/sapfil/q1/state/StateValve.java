package com.sapfil.q1.state;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.q1.layer.LayerType;
import com.sapfil.q1.object.GameObjects.GameObject;

/********************************
 * Created by Sapfil 
 * Edit 17.06.2017.
 * Notes
 ********************************/

public class StateValve extends com.sapfil.q1.state.State {

    public enum Status {
        CLOSING,
        PAUSE,
        OPENING
    }

    private com.sapfil.q1.state.State firstState;
    private com.sapfil.q1.state.State secondState;
    private GameObject[] valveParts = new GameObject[7];

    private Status currentStatus = Status.CLOSING;
    private float pauseTime = 0.5f;

    public StateValve(GameStateManager pGSM, com.sapfil.q1.state.State firstState, State secondState) {
        super(pGSM);
        this.firstState = firstState;
        this.secondState = secondState;
        focusSet(firstState.currentFocus);
            valveParts[0] = new GameObject.GameObjectBuilder()
                    .create(61000, layerMap.get(LayerType.MAIN))
                    .withXYcoords(firstState.currentFocus.x, firstState.currentFocus.y + 24 + 17, true)
                    .withSpeedXY(0, -34, true)
                    .build();
            valveParts[1] = new GameObject.GameObjectBuilder()
                    .create(61001, layerMap.get(LayerType.MAIN))
                    .withXYcoords(firstState.currentFocus.x - 40 - 30, firstState.currentFocus.y, true)
                    .withSpeedXY(40, 0, true)
                    .build();
            valveParts[2] = new GameObject.GameObjectBuilder()
                    .create(61002, layerMap.get(LayerType.MAIN))
                    .withXYcoords(firstState.currentFocus.x + 40 + 30, firstState.currentFocus.y, true)
                    .withSpeedXY(-40, 0, true)
                    .build();
            valveParts[3] = new GameObject.GameObjectBuilder()
                    .create(61003, layerMap.get(LayerType.MAIN))
                    .withXYcoords(firstState.currentFocus.x - 40 - 10, firstState.currentFocus.y - 24 + 7, true)
                    .withSpeedXY(40, 0, true)
                    .build();
            valveParts[4] = new GameObject.GameObjectBuilder()
                    .create(61004, layerMap.get(LayerType.MAIN))
                    .withXYcoords(firstState.currentFocus.x + 40 + 10, firstState.currentFocus.y - 24 + 7, true)
                    .withSpeedXY(-40, 0, true)
                    .build();

            valveParts[5] = new GameObject.GameObjectBuilder()
                    .create(61005, layerMap.get(LayerType.MAIN))
                    .withXYcoords(firstState.currentFocus.x - 40 - 15, firstState.currentFocus.y - 24 + 14 + 5, true)
                    .withSpeedXY(40, 0, true)
                    .build();
            valveParts[6] = new GameObject.GameObjectBuilder()
                    .create(61006, layerMap.get(LayerType.MAIN))
                    .withXYcoords(firstState.currentFocus.x + 40 + 15, firstState.currentFocus.y - 24 + 14 + 5, true)
                    .withSpeedXY(-40, 0, true)
                    .build();
        }


    @Override
    protected void inputHandler() {
    }

    @Override
    public void update(float dt) {
        switch (currentStatus){
            case CLOSING:{
                if (valveParts[0].getAbsY() > firstState.currentFocus.y + 24 - 17) {
                    firstState.update(dt);
                    layerMap.get(LayerType.MAIN).update(0.1f);
                } else {
                    valveParts[0].getLocalCoordinates().setY(firstState.currentFocus.y + 24 - 17);
                    valveParts[1].getLocalCoordinates().setX(firstState.currentFocus.x - 40 + 10);
                    valveParts[2].getLocalCoordinates().setX(firstState.currentFocus.x + 40 - 10);
                    valveParts[3].getLocalCoordinates().setX(firstState.currentFocus.x - 40 + 30);
                    valveParts[4].getLocalCoordinates().setX(firstState.currentFocus.x + 40 - 30);
                    valveParts[5].getLocalCoordinates().setX(firstState.currentFocus.x - 40 + 25);
                    valveParts[6].getLocalCoordinates().setX(firstState.currentFocus.x + 40 - 25);
                    for (GameObject part: valveParts){
                        part.getEntity().setSpeedXY(-part.getEntity().getSpeedX(), -part.getEntity().getSpeedY());
                        part.getLocalCoordinates().setX(part.getAbsX() - currentFocus.x + secondState.currentFocus.x);
                        part.getLocalCoordinates().setY(part.getAbsY() - currentFocus.y + secondState.currentFocus.y);
                    }

                    focusSet(secondState.currentFocus);
                    currentStatus = Status.PAUSE;
                }
                break;
            }
            case PAUSE:{
                if (pauseTime > 0)
                    pauseTime -= dt;
                else
                    currentStatus = Status.OPENING;
                break;
            }
            case OPENING:{
                if (valveParts[0].getAbsY() < secondState.currentFocus.y + 24 + 17) {
                    secondState.update(dt);
                    layerMap.get(LayerType.MAIN).update(0.1f);
                }
                else
                    gsm.set(this.secondState);
            }
        }
    }

    @Override
    public void render(SpriteBatch pSB) {

        camera.update();
        pSB.setProjectionMatrix(camera.combined);

        switch (currentStatus){
            case CLOSING:{
                firstState.render(pSB);
                break;
            }
            case OPENING: secondState.render(pSB);
        }
        layerMap.get(LayerType.MAIN).render(pSB);
    }

    @Override
    public void dispose() {
        valveParts[0].dispose(false);
    }

}
