package com.sapfil.q1.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import com.sapfil.q1.layer.LayerImpl;
import com.sapfil.q1.layer.LayerType;
import com.sapfil.q1.world.grid.WorldGrid;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.object.GFXObjects.AGFXO;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.object.PhysicsObjects.IHealthObject;
import com.sapfil.q1.UI.UI;
import com.sapfil.q1.world.grid.WorldGridImpl;

import java.util.Map;


/************************************************
 * Created by Sapfil on 16.09.2016.
 * Last edit: 02-10-2016 sBlasts added
 * Notes: Memory-loss check
 ***********************************************/
public class PlayState extends State {

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static UI ui;

    // ===========================================================
    // Fields
    // ===========================================================

    private BitmapFont techInfoFont;

    private WorldGridImpl worldGrid;

    private GameObject mHero;

    // ===========================================================
    // Constructors
    // ===========================================================

    public PlayState(GameStateManager pGSM) {
        super(pGSM);

        addHero();

        ui = new UI(layerMap.get(LayerType.UI));
        worldGrid = new WorldGridImpl("world_0_2", layerMap,this);

        techInfoFont = new BitmapFont();
        techInfoFont.getData().scaleX = 0.1f;
        techInfoFont.getData().scaleY = 0.1f;
        techInfoFont.setUseIntegerPositions(false);

        focusSet(mHero.getAbsX(), mHero.getAbsY());
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    @Override
    protected void inputHandler() {
        mouse.x = Gdx.input.getX();
        mouse.y = Gdx.input.getY();

        if (Gdx.input.justTouched()){

            if(mouse.x < 50 && mouse.y < 50) {
                ui.changeDebugMode();
                return;
            }
//            camera.unproject(mouse);
//            GameObject gameObject = new GameObject.GameObjectBuilder()
//                    .create(103, layerMap.get(LayerType.MAIN))
//                    .withXYcoords(mouse.x, mouse.y)
//                    .build();
//            worldGrid.adGameObject(gameObject);
        }

        if (Gdx.input.isTouched()){

            camera.unproject(mouse);

            if (!mHero.getHeroProperty().isBysy()) {
                {
                    IHealthObject touchedObject = layerMap.get(LayerType.MAIN).getHealthObjectCollection().getObjectContsinsPoint(mouse.x, mouse.y);
                    if (touchedObject != null) {
                        switch (touchedObject.getMotherObject().getTypeOfObject()) {
                            case 100: {
                                mHero.getHeroProperty().setMoveTargetObject(touchedObject.getMotherObject());
                                break;
                            } //asteroid
                            case 800: {
                                mHero.getHeroProperty().sendHeroToBase(touchedObject.getMotherObject().getBaseProperty().getEntrancepoint());
                            }    //base
                        }
                    } else {

                        mHero.getHeroProperty().setMoveTargetPoint(mouse.x, mouse.y);
                    }
                }
            }
        }
    }

    @Override
    public void update(float dt) {
        super.update(dt);

        //TODO move to state
        //TODO create half-consts
        //TODO add bias to borders
        focusSet(mHero.getAbsX(), mHero.getAbsY());

        AGFXO.screenLeftBorder = mHero.getAbsX() - GlobalConstants.WIDTH/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;
        AGFXO.screenRightBorder = mHero.getAbsX() + GlobalConstants.WIDTH/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;
        AGFXO.screenDownBorder = mHero.getAbsY() - GlobalConstants.HEIGHT/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;
        AGFXO.screenUpBorder = mHero.getAbsY() + GlobalConstants.HEIGHT/2 * GlobalConstants.GFX_TO_PHYS * GlobalConstants.ON_SCREEN_BIAS;

        worldGrid.update();  // updating world-grid - loading new world-cell and destroying far-cells

        ui.transponCoords( mHero.getAbsX(), mHero.getAbsY()); //TODO swimming button
        ui.update(dt);

        if (mHero.getHeroProperty().update(dt) == 10) {
            gsm.set(GameStateManager.stateType.BASE);
        }
    }


    @Override
    public void render(SpriteBatch pSB) {
        super.render(pSB);
        // *** tech-info
        techInfoRender(pSB);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public Vector2 getCenterCoords(){
        return new Vector2(mHero.getAbsX(), mHero.getAbsY());
    }

    private void addHero() {
            mHero = new GameObject.GameObjectBuilder()
                    .create(2, layerMap.get(LayerType.MAIN))
                    .withZcoord(1000)
                    .withXYcoords(0, 0)
                    .build();
    }

    private void techInfoRender(SpriteBatch pSB){
        float ux = currentFocus.x - (GlobalConstants.WIDTH / 2) / 10 + 5.2f;
        float ly = currentFocus.y + (GlobalConstants.HEIGHT / 2 - 40) / 10;

        pSB.begin();
        techInfoFont.draw(pSB, "FPS:       " + Gdx.graphics.getFramesPerSecond(),
                ux, ly);
        if (ui.getDebugMode() == UI.DebugMode.STATS) {

            techInfoFont.draw(pSB, "Hero spd:  " + mHero.getHealthObject().getBody().getLinearVelocity().len(),
                    ux, ly - 2);
            techInfoFont.draw(pSB, "Hero stts: " + mHero.getHeroProperty().getStatus(),
                    ux, ly - 4);

            int h = 6;
            for (Map.Entry<String, String> entry : layerMap.get(LayerType.MAIN).getStats().entrySet()){
                techInfoFont.draw(pSB, entry.getKey() + ": " + entry.getValue(),
                        ux, ly - h);
                h += 2;
            }
        }
        if (ui.getDebugMode() == UI.DebugMode.PHYSICS){
            layerMap.get(LayerType.MAIN).physicsDebugRender(camera);
        }
        pSB.end();
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================



}
