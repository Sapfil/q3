package com.sapfil.q1.layer;

public enum LayerType {

    BACK,
    MAIN,
    DEBUG,
    UI
}
