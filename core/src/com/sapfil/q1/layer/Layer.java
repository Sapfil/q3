package com.sapfil.q1.layer;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sapfil.q1.object.GFXObjects.ZSortedGFXCollecion;
import com.sapfil.q1.object.ObjectDependency.ChildrenCollection;
import com.sapfil.q1.object.PhysicsObjects.HealthObjectCollection;
import com.sapfil.q1.object.Position.LocalPositionCollection;

import java.util.Map;

import platform.Disposable;

/**
 * Игровой слой, полностью независимый от других слоев.
 *
 * Не должен ничего знать о других слоях. Но в случае необходимости косвенного влияния -
 * существет возможность получить коллекции объектов.
 */
public interface Layer extends Disposable {

    String getName();

    void render(SpriteBatch spriteBatch);

    void physicsDebugRender(Camera camera);

    void update(float dt);

    // TODO вероятно нужно отказываться от этих коллекций в этом интерфейса
    ZSortedGFXCollecion getzSortedGFXCollecion();

    LocalPositionCollection getLocalPositionCollection();

    ChildrenCollection getChildrenCollection();

    HealthObjectCollection getHealthObjectCollection();

    Map<String, String> getStats();

}
