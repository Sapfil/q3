package com.sapfil.q1.layer;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.PerformanceCounter;
import com.badlogic.gdx.utils.PerformanceCounters;
import com.sapfil.q1.object.GFXObjects.ZSortedGFXCollecion;
import com.sapfil.q1.object.ObjectDependency.ChildrenCollection;
import com.sapfil.q1.object.PhysicsObjects.HealthObjectCollection;
import com.sapfil.q1.object.Position.LocalPositionCollection;

import java.util.HashMap;
import java.util.Map;

/********************************
 * Created by Sapfil 
 * Edit 24.06.2017.
 * Notes
 ********************************/

public class LayerImpl implements Layer{

    public static LayerImpl defaultLayer;
    public static LayerImpl debugLayer;

    private ZSortedGFXCollecion zSortedGFXCollecion = new ZSortedGFXCollecion();
    private LocalPositionCollection localPositionCollection = new LocalPositionCollection();
    private ChildrenCollection childrenCollection = new ChildrenCollection();
    private HealthObjectCollection healthObjectCollection = new HealthObjectCollection();
    private Box2DDebugRenderer physicalRenderer = new Box2DDebugRenderer();
    private PerformanceCounters performanceCounters;
    private PerformanceCounter perfEntity, perfChildren, perfGfx, perfHealth, perfRender, perfDebugRender;
    private final String name;
    private final Map<String,String> statsMap = new HashMap<>();

    public LayerImpl(String name) {

        this.name = name;

        performanceCounters = new PerformanceCounters();
        perfEntity = performanceCounters.add("entity");
        perfChildren = performanceCounters.add("children");
        perfGfx = performanceCounters.add("gfx");
        perfHealth = performanceCounters.add("health");
        perfRender = performanceCounters.add("render");
    }

    public String getName() {
        return name;
    }

    public ZSortedGFXCollecion getzSortedGFXCollecion() {
        return zSortedGFXCollecion;
    }

    public LocalPositionCollection getLocalPositionCollection() {
        return localPositionCollection;
    }

    public ChildrenCollection getChildrenCollection() {
        return childrenCollection;
    }

    public HealthObjectCollection getHealthObjectCollection() {
        return healthObjectCollection;
    }

    public void update(float dt) {
        performanceCounters.tick(dt);
        perfEntity.start();
        localPositionCollection.update(dt);
        perfEntity.stop();
        perfChildren.start();
        childrenCollection.update(dt);
        perfChildren.stop();
        perfGfx.start();
        zSortedGFXCollecion.update(dt);
        perfGfx.stop();
        perfHealth.start();
        healthObjectCollection.update(dt);
        perfHealth.stop();
    }

    public void render(SpriteBatch spriteBatch) {
        perfRender.start();
        zSortedGFXCollecion.render(spriteBatch);
        perfRender.stop();
    }

    public void physicsDebugRender(Camera camera) {
        physicalRenderer.render(HealthObjectCollection.world, camera.combined);
    }

    public Map<String, String> getStats() {

        for (int i = 0; i < performanceCounters.counters.size; i++) {
            statsMap.put(
                    performanceCounters.counters.get(i).name,
                    String.format("%.3f", performanceCounters.counters.get(i).time.average * 1000)
                    );
        }

        return statsMap;
    }

    public void dispose() {
        zSortedGFXCollecion.dispose();
        localPositionCollection.dispose();
        childrenCollection.dispose();
        healthObjectCollection.dispose();
    }
}
