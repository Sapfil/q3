package com.sapfil.q1.db.api;

import java.util.Set;

/**
 * Отображение одной БД, содержащей несколько таблиц
 * Для текущих потребностей предполагается прогружать всю БД целиком.
 *
 * Например, это будет БД, содержащая все объекты уровня.
 *
 *
 *
 *
 * Для текущей XML-реализации БД предлагается такая огранизация:
 *
 * 1) XML содержит один верхнеуровневый тэг. Именем БД можно считать имя тэга.
 *
 * 2) Тэги второго уровня являются "таблицами". Имеют атрибуты - дефолтные значения всех параметров
 * хранящихся объектов. именем таблицы является имя тэга.
 *
 * 3) Тэги третьего уровня - записи в таблицах.
 * Их набор параметров в рамках одной таблицы долден быть одинаковым и совпадать с именами
 * дефолтных параметров в родительском тэге. Если параметра в дочернем тэше нет - берется параметр
 * из родительского.
 */
public interface DataBase {

    /**
     * @return имена всех таблиц в БД
     */
    Set<String> getTableNames();

    /**
     * @param tableName имя таблицы
     * @return таблица БД
     */
    DAO getDataBaseTable(String tableName);

    /**
     * Сохранить текущее состояние БД в файл
     */
    void saveDataBase();

}
