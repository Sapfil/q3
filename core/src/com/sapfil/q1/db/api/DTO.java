package com.sapfil.q1.db.api;

/**
 * Отображение одной записи в БД.
 *
 * Описание - см третий пункт в JavaDoc {@link DataBase}
 */
public interface DTO {

}
