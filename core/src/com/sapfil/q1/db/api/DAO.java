package com.sapfil.q1.db.api;

import java.util.Collection;

/**
 * Доступ к объектам одной из таблиц
 */
public interface DAO<K extends Key, E extends DTO> {

    /**
     * @return все записи таблицы
     * Может вернуть пустую коллекцию, не может вернуть null
     */
    Collection<E> getAll();

    /**
     * добавить запись в таблицу
     * @return true если удалось выполнить операцию, иначе false
     */
    boolean create(K key, E value);

    /**
     * Получить запись по зключу.
     *
     * @return запись по ключу.
     * если ключи не уникальны - будет вовзвращено одно из значений по совпадающему ключу
     * null, если значения нет
     */
    E read(K key);

    /**
     * Обновить состояние записи.
     * В случае если записи с таким ключом не было в таблице - метод отработает так же как create
     *
     * @return true если удалось выполнить операцию, иначе false
     */
    boolean update(K key, E actualEntry);

    /**
     * Удалить запись из таблицы
     * @return true если удалось выполнить операцию, иначе false
     */
    boolean delete(K key);
}
