package com.sapfil.q1.db.level.key;

import com.sapfil.q1.db.api.Key;

import java.util.Objects;

/**
 * Ключ для поиска в БД.
 *
 * Для упрощения используется и снаружи БД - как ключ кв картах хранения Java-объектов ячеек мира
 */
public class CellKey implements Key {

    public final float x;
    public final float y;

    public CellKey(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CellKey cellKey = (CellKey) o;
        return Float.compare(cellKey.x, x) == 0 &&
                Float.compare(cellKey.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
