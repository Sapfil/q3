package com.sapfil.q1.db.level.dao;

import com.sapfil.q1.db.api.DTO;

import java.util.Set;

public class CellDTO implements DTO {

    private final float x, y;
    private final Set<CellObjectDTO> cellObjectSet;

    public CellDTO(float x, float y, Set<CellObjectDTO> cellObjectSet) {
        this.x = x;
        this.y = y;
        this.cellObjectSet = cellObjectSet;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public Set<CellObjectDTO> getCellObjectSet() {
        return cellObjectSet;
    }
}
