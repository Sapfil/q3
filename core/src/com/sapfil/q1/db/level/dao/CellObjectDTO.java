package com.sapfil.q1.db.level.dao;

public class CellObjectDTO {

    public final float x, y, z;
    public final int id;

    public CellObjectDTO(int id, float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.id = id;
    }
}
