package com.sapfil.q1.db.level.dao;

import com.badlogic.gdx.Gdx;
import com.sapfil.q1.db.api.DAO;
import com.sapfil.q1.db.level.key.CellKey;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.sapfil.q1.db.core.Utils.parseFloatNodeWithDefault;
import static com.sapfil.q1.db.core.Utils.parseFloatWithDefault;
import static com.sapfil.q1.db.core.Utils.parseIntegerNodeWithDefault;

/**
 *  Интерпретация полученного rDoc.
 *  Парсинг XML-нод в игровые сущности - ячейки мира. Парсинг верхнеуровневый - есть второй "слой"
 *  парнсинга, который "разбирает" ноду каждой ячейки и создает игровые объеты.
 *  {@link CellDTO}
 *
 *  Может редактировать rDoc.
 *  Не включает в себя работу с файлом (чтение - запись).
 */
public class CellDAOService implements DAO<CellKey, CellDTO> {

    private final Node mainNode;
    private final Document rDoc;
    private final Map<CellKey, Node> xmlIndex = new HashMap<>();
    private final Map<CellKey, CellDTO> dtoStorage = new HashMap<>();

    public CellDAOService(Document rDoc, Node mainNode) {

        this.mainNode = mainNode;
        this.rDoc = rDoc;

        NodeList cellsList = mainNode.getChildNodes();

        for (int cellNumberInXML = 0; cellNumberInXML < cellsList.getLength(); cellNumberInXML++)
            if (cellsList.item(cellNumberInXML).getNodeType() == Node.ELEMENT_NODE) {
                CellDTO singleCellDTO = parseCellNode(cellsList.item(cellNumberInXML));

                CellKey cellKey = new CellKey(singleCellDTO.getX(), singleCellDTO.getY());
                xmlIndex.put(cellKey, cellsList.item(cellNumberInXML));
                dtoStorage.put(cellKey, singleCellDTO);

                Gdx.app.log("Sapfil_Log", "Cell added at " + singleCellDTO.getX() + " " + singleCellDTO.getY());
            }
    }

    @Override
    public Collection<CellDTO> getAll() {
        return dtoStorage.values();
    }

    @Override
    public boolean create(CellKey key, CellDTO value) {
        Element newCell = rDoc.createElement("NewCell");
        newCell.setAttribute("x",Float.toString(key.x));
        newCell.setAttribute("y",Float.toString(key.y));
        mainNode.appendChild(newCell);
        mainNode.appendChild(rDoc.createTextNode("\n"));
        newCell.appendChild(rDoc.createTextNode("\n"));
        for (CellObjectDTO cellObjectDTO : value.getCellObjectSet()){
            Element newObject = rDoc.createElement("obj");
            newObject.setAttribute("id", Integer.toString(cellObjectDTO.id));
            newObject.setAttribute("x",Float.toString(cellObjectDTO.x));
            newObject.setAttribute("y",Float.toString(cellObjectDTO.y));
            newCell.appendChild(newObject);
            newCell.appendChild(rDoc.createTextNode("\n"));
        }
        xmlIndex.put(key, newCell);
        dtoStorage.put(key, value);
        return true;
    }

    @Override
    public CellDTO read(CellKey key) {
        return dtoStorage.get(key);
    }

    @Override
    public boolean update(CellKey key, CellDTO actualEntry) {
        boolean delFlag = true;
        if (xmlIndex.get(key) != null) {
             delFlag = delete(key);
        }
        if (!actualEntry.getCellObjectSet().isEmpty()) {
            return delFlag && create(key, actualEntry);
        }
        return delFlag;
    }

    @Override
    public boolean delete(CellKey key) {
        mainNode.removeChild(xmlIndex.get(key));
        dtoStorage.remove(key);
        return true;
    }

    private CellDTO parseCellNode(Node cellXmlNode) {
        NamedNodeMap XMLattributesMap = cellXmlNode.getAttributes();
        Map<String, String> attributesMap = new HashMap<>();
        for (int i = 0; i < XMLattributesMap.getLength(); i++) {
            attributesMap.put(XMLattributesMap.item(i).getNodeName(), XMLattributesMap.item(i).getNodeValue());
        }

        float cellCenterX = parseFloatWithDefault(attributesMap.get("x"), 0);
        float cellCenterY = parseFloatWithDefault(attributesMap.get("y"), 0);

        // вложенные элементы

        NodeList cellsList = cellXmlNode.getChildNodes();

        Set<CellObjectDTO> cellObjectSet = new HashSet<>();

        for (int j = 0; j < cellsList.getLength(); j++)
            if (cellsList.item(j).getNodeType() == Node.ELEMENT_NODE) {

                NamedNodeMap objectEntryAttributes = cellsList.item(j).getAttributes();

                int id = parseIntegerNodeWithDefault(objectEntryAttributes.getNamedItem("id"), 0);
                float x = parseFloatNodeWithDefault(objectEntryAttributes.getNamedItem("x"), cellCenterX);
                float y = parseFloatNodeWithDefault(objectEntryAttributes.getNamedItem("y"), cellCenterY);
                float z = parseFloatNodeWithDefault(objectEntryAttributes.getNamedItem("z"), 0);

                if (id != 0) {
                    cellObjectSet.add(new CellObjectDTO(id, x, y, z));
                } else {
                    Gdx.app.log("WARN", "found object without ID attribute. Did not loaded from database");
                }
            }
        return new CellDTO(cellCenterX, cellCenterY, cellObjectSet);
    }
}
