package com.sapfil.q1.db.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.sapfil.q1.db.api.DataBase;
import com.sapfil.q1.db.api.DAO;
import com.sapfil.q1.db.level.dao.CellDAOService;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class LevelDB implements DataBase {

    public Document rDoc;

    private final Map<String, DAO> levelTables = new HashMap<>();
    private final String levelMainTableName = "cells";
    private final String levelName;

    public LevelDB(String levelName) {

        this.levelName = levelName;

        // подготовка JAVA-отображения
        DocumentBuilder db = null;
        try {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        // подготовка файла
        FileHandle inputFileHandler = Gdx.files.external(levelName);
        if (!inputFileHandler.exists()) {
            inputFileHandler = Gdx.files.internal(levelName);
        }

        // чтение файла
        InputStream mInputStream = inputFileHandler.read();

        // парсинг файла в JAVA_ отображение
        try {
            rDoc = db.parse(mInputStream);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        //TODO сейчас БД уровня содержит одну таблицу по-этому она просто лежит первой в XML
        Node cellTableNode = rDoc.getFirstChild();

        levelTables.put(levelMainTableName, new CellDAOService(rDoc, cellTableNode));

//        this.saveWorld();
    }

    @Override
    public Set<String> getTableNames() {
        return levelTables.keySet();
    }

    @Override
    public DAO getDataBaseTable(String tableName) {
        return levelTables.get(tableName);
    }

    @Override
    public void saveDataBase() {

        FileHandle outputFile = Gdx.files.external(levelName);

        if (!outputFile.exists())
            try {
                outputFile.file().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                {
                    Gdx.app.log("Sapfil_Log", "ERROR in saving Level. " + e.getMessage());
                }
            }

        Result output = new StreamResult(outputFile.file());
        Source input = new DOMSource(rDoc);
        Transformer transformer = null;

        try {
            transformer = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
            {
                Gdx.app.log("Sapfil_Log", "ERROR in saving Level. " + e.getMessage());
            }
        }

        try {
            transformer.transform(input, output);
        } catch (TransformerException e) {
            e.printStackTrace();
            {
                Gdx.app.log("Sapfil_Log", "ERROR in saving level. " + e.getMessage());
            }
        }
    }
}
