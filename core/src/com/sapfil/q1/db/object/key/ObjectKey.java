package com.sapfil.q1.db.object.key;

import com.sapfil.q1.db.api.Key;

import java.util.Objects;

public class ObjectKey implements Key {

    public int id;

    public ObjectKey(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectKey objectKey = (ObjectKey) o;
        return id == objectKey.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
