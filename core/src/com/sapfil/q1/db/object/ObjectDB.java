package com.sapfil.q1.db.object;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.sapfil.q1.db.api.DAO;
import com.sapfil.q1.db.api.DataBase;
import com.sapfil.q1.db.level.dao.CellDAOService;
import com.sapfil.q1.db.level.dao.CellDTO;
import com.sapfil.q1.db.level.key.CellKey;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


//TODO\
//TODO\
//TODO\
//TODO вычистить копипасту от ячеек (Cell)


/**
 * База дынных объетов .
 * Каждая "таблица" (каждый тэг второго уровня в XML) соответсвует одному PNG-файлу со спрайтами
 * //TODO убрать базу с "телами" в отдельную БД
 */
public class ObjectDB implements DataBase {

    public Document rDoc;

    private final Map<String, DAO> objectDbTables = new HashMap<>();
    private final String HERO_SHIPS = "hero-ships";
    private final String HERO_FUR = "hero-fur";
    private final String ASTEROIDS = "asteroids";
    private final String BASE = "base";
    private final String BLASTS = "blasts";
    private final String DROPS = "drops";
    private final String DEBUG = "debug";
    private final String VALVE = "valve";
    private final String PLAY_BACK = "playBackGround";
    private final String BASE_BACK = "baseBackGround";
    private final String START_BACK = "startBackGround";

    private final String BODIES = "bodies";

    public ObjectDB(String xmlFileName) {
        // подготовка JAVA-отображения
        DocumentBuilder db = null;
        try {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        // подготовка файла
        FileHandle inputFileHandler = Gdx.files.external(xmlFileName);
        if (!inputFileHandler.exists()) {
            inputFileHandler = Gdx.files.internal(xmlFileName);
        }

        // чтение файла
        InputStream mInputStream = inputFileHandler.read();

        // парсинг файла в JAVA_ отображение
        try {
            rDoc = db.parse(mInputStream);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        Node mainNode = rDoc.getFirstChild();

        NodeList cellsList = mainNode.getChildNodes();

        for (int cellNumberInXML = 0; cellNumberInXML < cellsList.getLength(); cellNumberInXML++)
            if (cellsList.item(cellNumberInXML).getNodeType() == Node.ELEMENT_NODE) {
                CellDTO singleCellDTO = parseCellNode(cellsList.item(cellNumberInXML));

                CellKey cellKey = new CellKey(singleCellDTO.getX(), singleCellDTO.getY());
                xmlIndex.put(cellKey, cellsList.item(cellNumberInXML));
                dtoStorage.put(cellKey, singleCellDTO);

                Gdx.app.log("Sapfil_Log", "Cell added at " + singleCellDTO.getX() + " " + singleCellDTO.getY());
            }

        levelTables.put(levelMainTableName, new CellDAOService(rDoc, cellTableNode));
    }



    @Override
    public Set<String> getTableNames() {
        return null;
    }

    @Override
    public DAO getDataBaseTable(String tableName) {
        return null;
    }

    @Override
    public void saveDataBase() {

    }
}
