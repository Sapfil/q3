package com.sapfil.q1.db.object.dao;

public class ObjectChildDTO {

    public final int id;
    public final int offsetX;
    public final int offsetY;

    public ObjectChildDTO(int id, int offsetX, int offsetY) {
        this.id = id;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }
}
