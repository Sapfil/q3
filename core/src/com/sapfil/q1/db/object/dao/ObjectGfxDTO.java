package com.sapfil.q1.db.object.dao;

public class ObjectGfxDTO {

    public final int textureCornerX ;
    public final int textureCornerY ;
    public final int width ;
    public final int height;
    public final int fps ;
    public final int framesInRow;
    public final int framesInColomn ;
    public final int origin_x ;
    public final int origin_y;
    public final String textureFileName;

    public ObjectGfxDTO(int textureCornerX, int textureCornerY, int width, int height, int fps, int framesInRow, int framesInColomn, int origin_x, int origin_y, String textureFileName) {
        this.textureCornerX = textureCornerX;
        this.textureCornerY = textureCornerY;
        this.width = width;
        this.height = height;
        this.fps = fps;
        this.framesInRow = framesInRow;
        this.framesInColomn = framesInColomn;
        this.origin_x = origin_x;
        this.origin_y = origin_y;
        this.textureFileName = textureFileName;
    }
}
