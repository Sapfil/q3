package com.sapfil.q1.db.object.dao;

import com.badlogic.gdx.physics.box2d.BodyDef;

public class ObjectBodyDTO {

    public final int health;
    public final int typeOfBlast;
    public final BodyDef.BodyType bodyType;
    public final int[] bodies;

    public ObjectBodyDTO(int health, int typeOfBlast, BodyDef.BodyType bodyType, int[] bodies) {
        this.health = health;
        this.typeOfBlast = typeOfBlast;
        this.bodyType = bodyType;
        this.bodies = bodies;
    }
}
