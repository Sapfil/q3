package com.sapfil.q1.db.object.dao;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.sapfil.q1.db.api.DAO;
import com.sapfil.q1.db.object.key.ObjectKey;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.sapfil.q1.db.core.Utils.parseIntegerWithDefault;

public class ObjectDAOService implements DAO<ObjectKey, ObjectDTO> {

    private final Node mainNode;
    private final Document rDoc;
    private final Map<ObjectKey, Node> xmlIndex = new HashMap<>();
    private final Map<ObjectKey, ObjectDTO> dtoStorage = new HashMap<>();

    public ObjectDAOService(Document rDoc, Node mainNode) {

        this.mainNode = mainNode;
        this.rDoc = rDoc;

        NodeList cellsList = mainNode.getChildNodes();

        for (int cellNumberInXML = 0; cellNumberInXML < cellsList.getLength(); cellNumberInXML++)
            if (cellsList.item(cellNumberInXML).getNodeType() == Node.ELEMENT_NODE) {
                ObjectDTO objectDTO = parseObjectNode(cellsList.item(cellNumberInXML));

                ObjectKey objectKey = new ObjectKey(objectDTO.id);
                xmlIndex.put(objectKey, cellsList.item(cellNumberInXML));
                dtoStorage.put(objectKey, objectDTO);

                Gdx.app.log("Sapfil_Log", "Object read from DB with ID " + objectKey.id);
            }
    }

    @Override
    public Collection<ObjectDTO> getAll() {
        return null;
    }

    @Override
    public boolean create(ObjectKey key, ObjectDTO value) {
        return false;
    }

    @Override
    public ObjectDTO read(ObjectKey key) {
        return null;
    }

    @Override
    public boolean update(ObjectKey key, ObjectDTO actualEntry) {
        return false;
    }

    @Override
    public boolean delete(ObjectKey key) {
        return false;
    }

    private ObjectDTO parseObjectNode(Node objectXmlNode) {
        NamedNodeMap XMLattributesMap = objectXmlNode.getAttributes();
        Map<String, String> attributesMap = new HashMap<>();
        for (int i = 0; i < XMLattributesMap.getLength(); i++) {
            attributesMap.put(XMLattributesMap.item(i).getNodeName(), XMLattributesMap.item(i).getNodeValue());
        }

        int id = parseIntegerWithDefault(attributesMap.get("id"), 0);
        int typeOfObject = parseIntegerWithDefault(attributesMap.get("too"), 0);

        ObjectGfxDTO objectGfxDTO = null;
        int width = parseIntegerWithDefault(attributesMap.get("w"), 0);
        if (width != 0) {
            int textureCornerX = parseIntegerWithDefault(attributesMap.get("x"), 0);
            int textureCornerY = parseIntegerWithDefault(attributesMap.get("y"), 0);
            int height = parseIntegerWithDefault(attributesMap.get("h"), 0);
            int fps = parseIntegerWithDefault(attributesMap.get("fps"), 0);
            int framesInRow = parseIntegerWithDefault(attributesMap.get("fw"), 1);
            int framesInColomn = parseIntegerWithDefault(attributesMap.get("fh"), 1);
            int origin_x = parseIntegerWithDefault(attributesMap.get("ox"), width / 2 / framesInRow);
            int origin_y = parseIntegerWithDefault(attributesMap.get("oy"), height / 2 / framesInColomn);
// TODO wtf ??????         int states = parseIntegerWithDefault(attributesMap.get("states"), 1);

            objectGfxDTO = new ObjectGfxDTO(textureCornerX,textureCornerY,width,height,
                    fps,framesInRow,framesInColomn,origin_x,origin_y,
                    mainNode.getLocalName() + ".png");
        }

        ObjectBodyDTO objectBodyDTO = null;
        int health = parseIntegerWithDefault(attributesMap.get("health"), 0);
        if (health != 0) {
            int typeOfBlast =  parseIntegerWithDefault(attributesMap.get("tob"), 0);
            int typeOfBody =parseIntegerWithDefault(attributesMap.get("type"), 0);
            int[] bodies = new int[parseIntegerWithDefault(attributesMap.get("bodies"), 0)];
            for (int k = 0; k < bodies.length; k++)
                bodies[k] = parseIntegerWithDefault(attributesMap.get("b" + k), 0);

            objectBodyDTO = new ObjectBodyDTO(health, typeOfBlast,
                    (typeOfBody == 0 ? BodyDef.BodyType.StaticBody :
                            typeOfBody == 1 ? BodyDef.BodyType.DynamicBody : BodyDef.BodyType.KinematicBody)
                    , bodies);
        }

        List<ObjectChildDTO> childrenList = null;
        int firstChildId =  parseIntegerWithDefault(attributesMap.get("id1"), 0);
        if (firstChildId > 0){
            childrenList = new LinkedList<>();
            int childCount = 1;
            while (parseIntegerWithDefault(attributesMap.get("id" + childCount), 0) > 0){
                childrenList.add(new ObjectChildDTO(
                        parseIntegerWithDefault(attributesMap.get("id" + childCount),0),
                        parseIntegerWithDefault(attributesMap.get("x" + childCount),0),
                        parseIntegerWithDefault(attributesMap.get("y" + childCount),0)
                ));
                childCount++;
            }
        }

        return new ObjectDTO(id, typeOfObject, objectGfxDTO, objectBodyDTO, childrenList);
    }
}
