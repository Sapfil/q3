package com.sapfil.q1.db.object.dao;

import com.sapfil.q1.db.api.DTO;

import java.util.List;

public class ObjectDTO implements DTO {

    public final int id;
    public final int typeOfObject;
    public final ObjectGfxDTO gfxDTO;
    public final ObjectBodyDTO bodyDTO;
    public final List<ObjectChildDTO> childDTOList;

    public ObjectDTO(int id, int typeOfObject, ObjectGfxDTO gfxDTO, ObjectBodyDTO bodyDTO, List<ObjectChildDTO> childDTOList) {
        this.id = id;
        this.typeOfObject = typeOfObject;
        this.gfxDTO = gfxDTO;
        this.bodyDTO = bodyDTO;
        this.childDTOList = childDTOList;
    }
}
