package com.sapfil.q1.object.PhysicsObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.sapfil.q1.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Prev edit: 16-10-2016
 * Last edit: 18-10-2016 extendins Bas-Obj-Col
 * Notes:
 ***********************************************/
public class HealthObjectCollection {

    //public static BasicObjectsCollection mCorpses;

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;
    public static World world = new World(new Vector2(0,0), true);
    static{
        world.setContactListener(new MyContactListener());
    }

    // ===========================================================
    // Fields
    // ===========================================================

//    protected HashMap<Integer, Array<IHealthObject>> objectArrayGroup;
    protected Array<IHealthObject> objectsArray;
    private int size = 0;

    // ===========================================================
    // Constructors
    // ===========================================================

    public HealthObjectCollection(){
        objectsArray = new Array<>();
        Gdx.app.log("Sapfil_Log", "New HealthObjectsCollection created");
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getTotalObjects() {
        return size;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    // overriding basic method for accessing to 2- and 3- updateCellsList commands
    public byte update (float dt){

        world.step(dt, 6, 2);

        for (IHealthObject entry : objectsArray) {

            switch (entry.update(dt)) {
                case 1: {
                }
                case 2: {
                }       // death moment
                case 3: {        // already dead object
                    entry.getMotherObject().dispose(true);
                    break;
                }     // dead Heath-object (Blast already created)
                case 0:         // (case 0) object alive - nothing
                default: {
                }
            }
        }

        return 0;
    }

    public void dispose(){
        for (IHealthObject entry : objectsArray)
            entry.dispose();
        objectsArray.clear();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public IHealthObject getObjectContsinsPoint(float pointX, float pointY){
        for (IHealthObject entry : objectsArray)
            // TODO separate collection for world-edges
            if (entry.getMotherObject().getTypeOfObject() > 0)
                for (Fixture fixture : entry.getFixtureList())
                    if (fixture.testPoint(pointX, pointY))
                        return entry;
        return null;
    }

//    public void addNewObject (Integer id, float x, float y){ this.addNewObject(id, x, y, 0, 0); }
    // TODO Delete type
    public void addNewObject (IHealthObject healthObject, int type){
        objectsArray.add(healthObject);
        size++;
    }

    // TODO Delete type
    public void deleteObject (IHealthObject healthObject, int type){
        objectsArray.removeValue(healthObject, true);
        healthObject.dispose();
        size--;
    }

    public void CreateCorpse(int deadType, float x, float y) {
        Gdx.app.log("Sapfil_Log", "deadType " + deadType);
    }
}
