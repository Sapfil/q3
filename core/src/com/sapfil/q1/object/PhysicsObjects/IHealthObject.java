package com.sapfil.q1.object.PhysicsObjects;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.sapfil.q1.object.GameObjects.GameObject;

/************************************************
 * Created by Sapfil on 16.01.2016.
 * Last edit: 16-10-2016
 * Notes:
 ***********************************************/
public interface IHealthObject{
        // ===========================================================
        // Constants
        // ===========================================================
        // ===========================================================
        // Getter & Setter
        // ===========================================================

        float getHealth();
        int getTypeOfBlast();
        Body getBody();
        java.util.List<Fixture> getFixtureList();

        // ===========================================================
        // Methods
        // ===========================================================
        byte update(float dt);
//        void resetBody();
        void reduceHealth(float healthToReduce1);
        void kill();
        public GameObject getMotherObject() ;

        void dispose();
//        void resetObject(GameObject gameObject, GameObject parent);
}
