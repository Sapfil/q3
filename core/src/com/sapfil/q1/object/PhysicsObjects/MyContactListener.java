package com.sapfil.q1.object.PhysicsObjects;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.object.specialObjects.Hero;

import static com.sapfil.q1.object.specialObjects.Hero.Status.STAY;

/********************************
 * Created by Sapfil 
 * Edit 07.05.2017.
 * Notes
 ********************************/

public class MyContactListener implements ContactListener {

    GameObject objectA;
    GameObject objectB;

    private void determineObjects(Contact contact){
        Fixture fixtureA;
        Fixture fixtureB;

        if ( ( (com.sapfil.q1.object.GameObjects.GameObject) (contact.getFixtureA().getUserData())).getTypeOfObject() < ( (com.sapfil.q1.object.GameObjects.GameObject) (contact.getFixtureB().getUserData())).getTypeOfObject()){
            fixtureA = contact.getFixtureA();
            fixtureB = contact.getFixtureB();
        }
        else{
            fixtureB = contact.getFixtureA();
            fixtureA = contact.getFixtureB();
        }

        objectA = (GameObject) fixtureA.getUserData();
        objectB = (GameObject) fixtureB.getUserData();
    }


    @Override
    public void beginContact(Contact contact) {

        determineObjects(contact);

        // Hero with asteroid collision
        try {
            if (objectA.getTypeOfObject() == 1 && objectB.getTypeOfObject() == 100) {

//                objectA.getHealthObject().kill();
                objectA.getHeroProperty().setStatus(STAY);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        if( objectA.getTypeOfObject() == 0)
            if( objectB.getTypeOfObject() == 1)
                if( !objectB.getHeroProperty().isBysy())
                    objectB.getHeroProperty().setStatus(STAY);


        if( objectA.getTypeOfObject() == 1)
            if( !objectA.getHeroProperty().isBysy())
                objectA.getHeroProperty().setStatus(STAY);
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

        determineObjects(contact);

        // Hero entering and exiting base
        if( objectA.getTypeOfObject() == 1 && objectB.getTypeOfObject() == 800 )
            if( objectA.getHeroProperty().isBysy()) {
                contact.setEnabled(false);
            }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
