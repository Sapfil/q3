package com.sapfil.q1.object.PhysicsObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.sapfil.q1.XML.DataBaseFromXML;
import com.sapfil.q1.XML.ShapeDefFromXML;
import com.sapfil.q1.object.GameObjects.GameObject;

import java.util.LinkedList;
import java.util.List;

import static com.sapfil.q1.GlobalConstants.GFX_TO_PHYS;


/************************************************
 * Created by Sapfil on 16.01.2016.
 * Prev edit: 09-10-2016
 * Last edit: 20-10-2016 GFX/PHYS separation
 * Last edit: 18-02-2017 Self creating blasts
 * Notes:
 ***********************************************/
public class HealthObject implements IHealthObject {
    // ===========================================================
    // Constants
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    private final float maxHealth;
    private float health;
    private Body body;
    private List<Fixture> fixtureList = new LinkedList<>();
    private boolean isAlive = true;

    private byte updateReturn = 0;

    private com.sapfil.q1.object.GameObjects.GameObject mothership;


    // ===========================================================
    // Constructors
    // ===========================================================

    public HealthObject(com.sapfil.q1.object.GameObjects.GameObject mothership, com.sapfil.q1.object.GameObjects.GameObject parentObject) {

        this.mothership = mothership;
        this.health = sDB.healthPartMap.get(mothership.getID()).health;
        this.maxHealth = health;

        if(sDB.healthPartMap.get(mothership.getID()).bodies.length > 0){

            //  *** Body definition
            BodyDef bodyDef = new BodyDef();
            bodyDef.type = sDB.healthPartMap.get(mothership.getID()).type;
            bodyDef.position.set( mothership.getAbsX(), mothership.getAbsY());

            //  *** Body creation
            float xCorrection = 0, yCorrection = 0;
            if (parentObject == null)
                body = com.sapfil.q1.object.PhysicsObjects.HealthObjectCollection.world.createBody(bodyDef);
            else {
                body = parentObject.getHealthObject().getBody();
                while(parentObject.getDependencyObject() != null)
                    parentObject = parentObject.getDependencyObject().getParent();
                xCorrection = mothership.getAbsX() - parentObject.getAbsX();
                yCorrection = mothership.getAbsY() - parentObject.getAbsY();
                xCorrection *= 1/GFX_TO_PHYS;
                yCorrection *= 1/GFX_TO_PHYS;
            }

            FixtureDef fixtureDef;

            for (int f = 0; f < sDB.healthPartMap.get(mothership.getID()).bodies.length; f++) {

                Fixture fixture;

                if (sDB.bodiesMap.get((sDB.healthPartMap.get(mothership.getID()).bodies[f])).getType() == ShapeDefFromXML.Type.SPHERE) {
                    CircleShape circleShape = new CircleShape();
                    circleShape.setPosition(new Vector2(
                            ((Vector3) (sDB.bodiesMap.get((sDB.healthPartMap.get(mothership.getID()).bodies[f])).getData())).x + xCorrection,
                            ((Vector3) (sDB.bodiesMap.get((sDB.healthPartMap.get(mothership.getID()).bodies[f])).getData())).y + yCorrection
                    ));
                    circleShape.setRadius(
                            ((Vector3) (sDB.bodiesMap.get((sDB.healthPartMap.get(mothership.getID()).bodies[f]))
                            .getData())).z * GFX_TO_PHYS);
                    //  *** Fixture definition
                    fixtureDef = new FixtureDef();
                    fixtureDef.shape = circleShape;
                    if (parentObject == null)
                        fixtureDef.density = 0.1f;
                    else
                        fixtureDef.density = 0.0f;
                    fixtureDef.friction = 0.4f;
                    fixtureDef.restitution = 0.6f;
                    //  *** Fixture creation
                    fixture = body.createFixture(fixtureDef);
                    fixture.setUserData(mothership);
                    circleShape.dispose();
                } else {
                    PolygonShape polygonShape = new PolygonShape();
                    float[] verticies = (float[]) (sDB.bodiesMap.get((sDB.healthPartMap.get(mothership.getID()).bodies[f])).getData());
                    for (int i = 0; i < verticies.length; i = i + 2) {
                        verticies[i] += xCorrection;
                        verticies[i + 1] += yCorrection;
                        verticies[i] *= GFX_TO_PHYS;
                        verticies[i + 1] *= GFX_TO_PHYS;
                    }

                    polygonShape.set(verticies);
                    // *** Fixture definition
                    fixtureDef = new FixtureDef();
                    fixtureDef.shape = polygonShape;
                    fixtureDef.density = 0.5f;
                    fixtureDef.friction = 0.4f;
                    fixtureDef.restitution = 0.1f;
                    //  *** Fixture creation
                    fixture = body.createFixture(fixtureDef);
                    fixture.setUserData(mothership);
                    polygonShape.dispose();
                }

                if (fixture != null)
                    fixtureList.add(fixture);
            }

            if (body.getType() == BodyDef.BodyType.KinematicBody && parentObject == null){
                body.setLinearVelocity(mothership.getEntity().getSpeedX(), mothership.getEntity().getSpeedY());
                body.setAngularVelocity(mothership.getEntity().getRotationSpeed() * MathUtils.degreesToRadians);

            }

        }
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public float getHealth(){
        return health;
    }

    @Override
    public int getTypeOfBlast() {
        return sDB.healthPartMap.get(mothership.getID()).type_of_blast;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public List<Fixture> getFixtureList() {
        return fixtureList;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public byte update(float dt){

        if (this.mothership.getTypeOfObject() < 0) //dead object
            updateReturn = 3;

        if (this.health <= 0 && mothership.getTypeOfObject() >= 0){
            this.kill();	// death
            updateReturn = 2;
        }
         return updateReturn;
    }

    public void reduceHealth(float healthToReduce1) {
        health -= healthToReduce1;
    }

    public void kill(){
        health = 0;
        isAlive = false;
    }

    @Override
    public GameObject getMotherObject() {
        return mothership;
    }

    @Override
    public void dispose() {
        try {
            body.destroyFixture(body.getFixtureList().first());
        }
        catch (NullPointerException e) {
            Gdx.app.log("ERROR", "Error in disposing HealthObject", e);
        }
        HealthObjectCollection.world.destroyBody(body);

        this.mothership = null;
        body = null;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public Body getBody() {
        return this.body;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
