package com.sapfil.q1.object.GameObjects;

/************************************************
 * Created by Sapfil on 13.01.2016.
 * Prev edit: 02-10-2016
 * Last edit: 18-10-2016 converting to Array<Iobject>
 * Notes:
 ***********************************************/

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.XML.DataBaseFromXML;

public class GameObjectsCollection implements GlobalConstants {

    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    protected Array<com.sapfil.q1.object.GameObjects.GameObject> objectsArray;

    // ===========================================================
    // Constructors
    // ===========================================================

    public GameObjectsCollection() {
//        Gdx.app.log("Sapfil_Log", "New GameObjectsCollection created");
        objectsArray = new Array<com.sapfil.q1.object.GameObjects.GameObject>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getTotalObjects() {
        return (objectsArray.size);
    }

    public com.sapfil.q1.object.GameObjects.GameObject getObjectByIndex(int i) {
        return this.objectsArray.get(i);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt) {
        return 0;
    }

    public void render(SpriteBatch pSB) {
    }

    public void dispose() {
        if (objectsArray.size > 0) {
            while (objectsArray.size > 1)
                objectsArray.get(0).disposeUsingBatch();
            objectsArray.get(0).dispose();
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject(com.sapfil.q1.object.GameObjects.GameObject gameObject){
            this.objectsArray.add(gameObject);
    }

    protected void deleteObject(int i) {
        this.objectsArray.get(i).dispose();
        objectsArray.removeIndex(i);
    }

    protected void deleteObject(GameObject iGameObject){
        this.objectsArray.removeValue(iGameObject, true);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}