package com.sapfil.q1.object.GameObjects;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sapfil on 11.10.2017.
 */

public class GameObjectsPool {

    private HashMap<Integer, List<com.sapfil.q1.object.GameObjects.GameObject>> poolMap = new HashMap<>();

    public void put(com.sapfil.q1.object.GameObjects.GameObject gameObject){
        int id = gameObject.getID();

        if (poolMap.get(id) == null)
            poolMap.put(id, new LinkedList<com.sapfil.q1.object.GameObjects.GameObject>());

        poolMap.get(id).add(gameObject);
    }

    public com.sapfil.q1.object.GameObjects.GameObject get(int id){
        if (poolMap.get(id) == null)
            return null;
        else if (poolMap.get(id).size() == 0)
            return null;
        else {
            GameObject gameObject = poolMap.get(id).get(0);
            poolMap.get(id).remove(0);
            return gameObject;
        }
    }
}
