package com.sapfil.q1.object.GameObjects;

import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.layer.LayerImpl;
import com.sapfil.q1.object.PhysicsObjects.HealthObject;
import com.sapfil.q1.object.PhysicsObjects.IHealthObject;
import com.sapfil.q1.object.specialObjects.Base;
import com.sapfil.q1.object.specialObjects.Hero;
import com.sapfil.q1.object.GFXObjects.AGFXO;
import com.sapfil.q1.object.ObjectDependency.IDependency;
import com.sapfil.q1.object.ObjectDependency.PositionDependency;
import com.sapfil.q1.object.Position.Coordinates;
import com.sapfil.q1.object.Position.IPosition;
import com.sapfil.q1.object.Position.PositionUpdater;
import com.sapfil.q1.XML.DataBaseFromXML;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import static com.sapfil.q1.GlobalConstants.GFX_TO_PHYS;

/************************************************
 * Created by Sapfil on 26.02.2017.
 * Last edit: 26.03.2017 Complex Refactor
 * Notes:
 ***********************************************/
public class GameObject {

    // ===========================================================
    // Constants and static fields
    // ===========================================================
    public static DataBaseFromXML sDB;
    public static GameObjectsPool pool;

    // ===========================================================
    // Fields
    // ===========================================================

    private int id;
    private int typeOfObject;

    private GameObjectsCollection gameObjectsCollection;
    private Set<Consumer<GameObject>> disposeConsumerSet;

    // Global parameters
    private Coordinates localCoordinates = new Coordinates();
    private Coordinates coordinatesCorrection = new Coordinates();

    // Plugins
    private IPosition entity;
    private AGFXO gfxObject;
    private IDependency positionDependency;
    private IHealthObject healthObject;

    private Hero heroProperty;
    private Base baseProperty;

    private Layer layer;

    public Layer getLayer() {
        return layer;
    }
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // Global parameters
    public Coordinates getLocalCoordinates() {
        return localCoordinates;
    }
    public void setLocalCoordinates(Coordinates coordinates) {
        localCoordinates = coordinates;
    }
    public com.sapfil.q1.object.Position.Coordinates getCoordinatesCorrection() {
        return coordinatesCorrection;
    }
    public void setCoordinatesCorrection(Coordinates coordinates){
        coordinatesCorrection = coordinates;
    }

    // Plugins
    public IPosition getEntity() {
        return entity;
    }
    public com.sapfil.q1.object.GFXObjects.AGFXO getGfxObject() {
        return gfxObject;
    }
    public IDependency getDependencyObject() {return  positionDependency;}
    public IHealthObject getHealthObject(){return healthObject;}
    public Hero getHeroProperty(){return heroProperty;}
    public Base getBaseProperty() {return baseProperty;}
    public void setBaseProperty(Base base) {baseProperty = base; }

    //shortcut-getters
    public int getID(){ return id;}
    public int getTypeOfObject() {return typeOfObject;}

    public float getAbsX(){
        return localCoordinates.getX() + coordinatesCorrection.getX();
    }
    public float getAbsY(){
        return localCoordinates.getY() + coordinatesCorrection.getY();
    }
    public float getAbsZ(){
        return localCoordinates.getZ() + coordinatesCorrection.getZ();
    }

    public float getAbsRotation(){return (localCoordinates.getRotation() + coordinatesCorrection.getRotation());}

    public void setVisible (boolean isVisible){
        if (gfxObject != null)
            gfxObject.setEnabled(isVisible);
        if (positionDependency != null && !positionDependency.getChildren().isEmpty())
            for (GameObject child : positionDependency.getChildren())
                child.setVisible(isVisible);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public void dispose(){
        this.dispose(false);
    }

    public void disposeUsingBatch(){
        if (gfxObject != null) {
            layer.getzSortedGFXCollecion().deletObjectUsingBatch(gfxObject);
        }
        if (healthObject != null) {
            layer.getHealthObjectCollection().deleteObject(healthObject, typeOfObject);
        }
        this.disposeInner();
    }

    private void disposeInner(){
        healthObject = null;
        if (positionDependency != null) {
            for (GameObject child : positionDependency.getChildren())
                child.dispose();
            layer.getChildrenCollection().deleteObject(positionDependency);
            positionDependency = null;
        }
        if (entity != null)
            layer.getLocalPositionCollection().unregister(entity);

        // If delete this - will be memory leak by duplicating recreated objects
        if (gameObjectsCollection != null)
            gameObjectsCollection.deleteObject(this);
        pool.put(this);
    }

    public void dispose(boolean createBlast) {
            for (Consumer<GameObject> disposConsumer: disposeConsumerSet){
                disposConsumer.accept(this);
            }
            if (healthObject != null) {
                layer.getHealthObjectCollection().deleteObject(healthObject, typeOfObject);
                if (createBlast && gameObjectsCollection != null && sDB.healthPartMap.get(id).type_of_blast != 0)
                    new GameObject.GameObjectBuilder()
                            .create(sDB.healthPartMap.get(id).type_of_blast, layer)
                            .withXYcoords(this.getAbsX(), this.getAbsY(), true)
                            .insertToCollection(gameObjectsCollection)
                            .build();
            }
            if (gfxObject != null) {
                layer.getzSortedGFXCollecion().deleteObject(gfxObject);
            }
            disposeInner();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public static class GameObjectBuilder {

        private GameObject gameObject;
        private boolean specialEntityOriginSet = false;
        private boolean xyRecalculationByGFXcenter = true;
        private boolean staticGFX = false;
        private GameObject parent = null;
        private Set<Consumer<GameObject>> disposeConsumerSet = new HashSet<>();

        private boolean pooled = false;

        public GameObjectBuilder create(int id, Layer layer){
            gameObject = pool.get(id);
            if (gameObject == null) {
                gameObject = new GameObject();
                gameObject.id = id;
                gameObject.typeOfObject = sDB.basicPartMap.get(id).type_of_object;
                gameObject.entity = new com.sapfil.q1.object.Position.PositionUpdater(gameObject);
            }
            else
                pooled = true;
            gameObject.layer = layer;
            return this;
        }

        public GameObjectBuilder createStatic(int id, LayerImpl layer){

            gameObject = pool.get(id);
            staticGFX = true;
            if (gameObject == null) {
                gameObject = new GameObject();
                gameObject.id = id;
                gameObject.typeOfObject = sDB.basicPartMap.get(id).type_of_object;
                gameObject.entity = new PositionUpdater(gameObject);
            }
            else
                pooled = true;
            gameObject.layer = layer;
            return this;
        }

        public GameObjectBuilder addDisposeConsumer(Consumer<GameObject> disposeConsumer){
            disposeConsumerSet.add(disposeConsumer);
            return this;
        }

        // collection
        public GameObjectBuilder insertToCollection(GameObjectsCollection gameObjectsCollection){ gameObject.gameObjectsCollection = gameObjectsCollection; return this;}

        // global parameters
        public GameObjectBuilder withRotation(float pRotation)              {gameObject.getLocalCoordinates().setRotation(pRotation); return this;}
        public GameObjectBuilder withXYcoords(float x, float y)             {return this.withXYcoords(x,y, true);}
        public GameObjectBuilder withXYcornerCoords(float x, float y)       {gameObject.getLocalCoordinates().setX(x); gameObject.getLocalCoordinates().setY(y);  xyRecalculationByGFXcenter = false; return this;}
        public GameObjectBuilder withZcoord(float z)                        {gameObject.getLocalCoordinates().setZ(z);return  this;}

        // entity parameters
        public GameObjectBuilder withScale(float sX, float sY)              {gameObject.entity.setScale(sX, sY); return this;}
        public GameObjectBuilder withSpeedXY(float pVX, float pVY)          {return this.withSpeedXY(pVX,pVY, false);}
        public GameObjectBuilder withSpeedV(float pV)                       {gameObject.entity.setSpeedV(pV); return this;}
        public GameObjectBuilder withRotationSpeed(float pRotaionSpeed)     {gameObject.entity.setRotationSpeed(pRotaionSpeed); return this;}
        public GameObjectBuilder withOriginXY(float originX, float originY) {return this.withOriginXY(originX,originY,false);}

        public GameObjectBuilder withSpeedXY(float x, float y, boolean realCoords) {
            if (realCoords)gameObject.entity.setSpeedXY(x, y);
            else gameObject.entity.setSpeedXY(x * GFX_TO_PHYS, y * GFX_TO_PHYS);
            return this;
        }
        public GameObjectBuilder withXYcoords(float x, float y, boolean physCoords) {
            if (physCoords){gameObject.getLocalCoordinates().setX(x); gameObject.getLocalCoordinates().setY(y);}
            else {gameObject.getLocalCoordinates().setX(x * GFX_TO_PHYS); gameObject.getLocalCoordinates().setY(y * GFX_TO_PHYS);}
            return this;
        }

        public GameObjectBuilder withOriginXY(float originX, float originY, boolean realCoords) {
            if (realCoords) gameObject.entity.setOriginXY(originX, originY);
            else gameObject.entity.setOriginXY(originX * GFX_TO_PHYS, originY * GFX_TO_PHYS);
            specialEntityOriginSet = true;
            return this;
        }

        //dependency parameters
        public GameObjectBuilder setParent(GameObject parent)              {this.parent = parent; return this;}

        public GameObject build() {
            {
                gameObject.disposeConsumerSet = disposeConsumerSet;

                if (!specialEntityOriginSet)
                    gameObject.entity.setOriginXY(sDB.basicPartMap.get(gameObject.getID()).origin_x * GFX_TO_PHYS,
                            sDB.basicPartMap.get(gameObject.getID()).origin_y * GFX_TO_PHYS);

                // dependency build
                if (parent != null) {
                    gameObject.positionDependency = new com.sapfil.q1.object.ObjectDependency.PositionDependency(parent, gameObject);
                    if (parent.getDependencyObject() == null)
                        parent.positionDependency = new com.sapfil.q1.object.ObjectDependency.PositionDependency(null, parent);
                    parent.getDependencyObject().attachChild(gameObject);
                }

                // body build
                if (sDB.healthPartMap.get(gameObject.id) != null) {
                        gameObject.healthObject = new HealthObject(gameObject, parent);
                }


                //TODO withoutGFX
                // gfx build and entity build-end (after dependancy and HO)
                if (staticGFX)
                    gameObject.gfxObject = new com.sapfil.q1.object.GFXObjects.AGFXO.GfxObjectBuilder()
                            .createStaticGFxObjectEntity(gameObject.id, gameObject)
                            .gfxZcoord(gameObject.getLocalCoordinates().getZ())
                            .build();
                else
                    gameObject.gfxObject = new AGFXO.GfxObjectBuilder()
                            .createGFxObjectEntity(gameObject.id, gameObject)
                            .gfxZcoord(gameObject.getLocalCoordinates().getZ())
                            .build();

                if (!xyRecalculationByGFXcenter) {
                    gameObject.getLocalCoordinates().setX(gameObject.getLocalCoordinates().getX() + gameObject.getGfxObject().getGfxCenterX());
                    gameObject.getLocalCoordinates().setY(gameObject.getLocalCoordinates().getY() + gameObject.getGfxObject().getGfxCenterY());
                }

                // special build
                if (gameObject.id < 8)
                    gameObject.heroProperty = new Hero(gameObject);
                if (gameObject.id == 800)
                    gameObject.baseProperty = new Base(gameObject);

                // entity registration - not need to updateCellsList coordinates if Body exists.
                if (!staticGFX)
                    if (sDB.healthPartMap.get(gameObject.id) == null) {
                    try {
                        gameObject.layer.getLocalPositionCollection().register(gameObject.entity);
                    }
                    catch (NullPointerException e){
                        System.out.println();
                    }

                    }

                // gfx registration
                gameObject.layer.getzSortedGFXCollecion().addNewObject(gameObject.gfxObject);


                // body registration
                if (gameObject.healthObject != null)
                    gameObject.layer.getHealthObjectCollection().addNewObject(gameObject.healthObject, gameObject.typeOfObject);

                if (gameObject.gameObjectsCollection != null)
                    gameObject.gameObjectsCollection.addNewObject(gameObject);

                if (sDB.dependMap.get(gameObject.getID()) != null) {
                    if (gameObject.positionDependency == null)
                        gameObject.positionDependency = new PositionDependency(null, gameObject);
                    for (DataBaseFromXML.DependancySample dependancySample : sDB.dependMap.get(gameObject.getID()))
                        gameObject.positionDependency.attachChild(
                            new GameObject.GameObjectBuilder()
                                .create(dependancySample.dependantID, gameObject.layer)
                                .withXYcoords(dependancySample.x, dependancySample.y)
                                .insertToCollection(gameObject.gameObjectsCollection)
                                .withZcoord(gameObject.getLocalCoordinates().getZ())
                                .setParent(gameObject)
                                .build()
                        );
                }

                //dependency registration
                if (gameObject.positionDependency != null) {
                    gameObject.layer.getChildrenCollection().addNewObject(gameObject.positionDependency);
                    //base components
                    if (gameObject.getTypeOfObject() == 800) {
                        while (parent.getDependencyObject() != null)
                            parent = parent.getDependencyObject().getParent();
                        gameObject.baseProperty = parent.getBaseProperty();
                        if (gameObject.getID() == 812)
                            gameObject.baseProperty.setEntrancePosint(gameObject);
                        if (gameObject.getID() == 810)
                            gameObject.baseProperty.setDockPoint(gameObject);
                    }
                }
                return gameObject;
            }
        }
    }
}