package com.sapfil.q1.object.specialObjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.sapfil.q1.HeroConstants;
import com.sapfil.q1.object.GFXObjects.AGFXO;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.state.PlayState;

import static com.sapfil.q1.HeroConstants.AIM_ANGLE_SIN_MAX_ERROR;


/************************************************
 * Created by Sapfil on 27.12.2015.
 * Prev edit: 16-10-2016
 * Prev edit: 20-10-2016 GFX/PHYS separation
 * Last edit: 22-03-2017 components refactor
 * Notes: in "ACTIVE CODING" status
 ***********************************************/
public class Hero{
    // ===========================================================
    // Constants
    // ===========================================================

    public enum Status {
        MOVING_TO_OBJECT,
        MOVING_TO_BASE,
        MOVING_TO_EMPTY_POINT,
        STAY_AND_AIM,
        STAY_AND_MINE,
        ENTERING_BASE_AIMING,
        ENTERING_BASE_MOVING,
        LEAVING_BASE,
        STAY_IN_BASE,
        STAY
        }

    final static int HERO_WALK_GFX_STATES = 8;
    final static int HERO_STAY_GFX_STATES = 8;
    final static int HERO_MINE_GFX_STATES = 8;
    final static int HERO_STAY_SPRITE_INIT_ID = 10;
    final static int HERO_WALK_SPRITE_INIT_ID = 20;
    final static int HERO_MINE_SPRITE_INIT_ID = 30;

    // ===========================================================
    // Fields
    // ===========================================================

    private float target_D = 0, target_A, target_X, target_Y;
    private Status mCurrentStatus = Status.STAY;
    private Status mNextStatus = Status.STAY;
    private float BrakeDistance = HeroConstants.EMPTY_POINT_STOP_DISTANCE;

    private GameObject pTarget = null;
    private GameObject mothership;
    private Body body;

    private int rotationFrame = 1;
    private AGFXO currentSprite;

    private AGFXO[] walkStateSprite = new AGFXO[HERO_WALK_GFX_STATES];
    private AGFXO[] stayStateSprite = new AGFXO[HERO_STAY_GFX_STATES];
    private AGFXO[] mineStateSprite = new AGFXO[HERO_MINE_GFX_STATES];

    // ===========================================================
    // Constructors
    // ===========================================================

    public Hero(GameObject mothership){
        this.mothership = mothership;
        body = mothership.getHealthObject().getBody();
        mothership.getGfxObject().setPseudoRotate(true);
        this.mothership.getGfxObject().setEnabled(false);

        initSpriteArray(stayStateSprite, HERO_STAY_SPRITE_INIT_ID);
        initSpriteArray(walkStateSprite, HERO_WALK_SPRITE_INIT_ID);
        initSpriteArray(mineStateSprite, HERO_MINE_SPRITE_INIT_ID);

        currentSprite = stayStateSprite[rotationFrame];
        setCurrentRotationFrame(true);
    }

    private void initSpriteArray(AGFXO[] array, int initId){
        for (int i = 0; i < array.length; i++){
            array[i] = new AGFXO.GfxObjectBuilder()
                    .createGFxObjectEntity(initId + i, mothership)
                    .gfxZcoord(mothership.getLocalCoordinates().getZ())
                    .gfxDisable()
                    .build();
            array[i].setPseudoRotate(true);
            mothership.getLayer().getzSortedGFXCollecion().addNewObject(array[i]);
        }
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public void setStatus(Status s){

        if (mCurrentStatus != Status.LEAVING_BASE) {
            switch (s) {
                case MOVING_TO_EMPTY_POINT: { PlayState.ui.showFreeSpaceTouch(target_X, target_Y);  break;                }
                case STAY_AND_MINE: {stopAngularMovement();}
                case STAY_AND_AIM: {stopAngularMovement(); stopLinearMovement(); break;}
                case MOVING_TO_OBJECT: {  PlayState.ui.showObjectTouch(target_X, target_Y);  break;  }
                case MOVING_TO_BASE: {  PlayState.ui.showBaseTouch(target_X, target_Y);  break;  }
                case LEAVING_BASE: { setTargetCoords(pTarget.getBaseProperty().getEntrancepoint().getAbsX(), pTarget.getBaseProperty().getEntrancepoint().getAbsY()); }
                case STAY: {
                    PlayState.ui.hideTouches();
                    stopAngularMovement();
                }
                default:
            }

            mCurrentStatus = s;
            this.setCurrentRotationFrame(true);
        }
        else {
            mNextStatus = s;
        }
    }
    public Status getStatus (){return mCurrentStatus; }

    public void sendHeroToBase(GameObject entrancePoint){
        setTargetObject(entrancePoint);
        BrakeDistance = HeroConstants.EMPTY_POINT_STOP_DISTANCE;
        setStatus(Status.MOVING_TO_BASE);
    }

    public void setMoveTargetObject(GameObject pTargetObject){
        setTargetObject(pTargetObject);
//        try {
            BrakeDistance = HeroConstants.DRILL_MAX_DISTANCE + pTargetObject.getHealthObject().getBody().getFixtureList().first().getShape().getRadius()
                    + body.getFixtureList().first().getShape().getRadius();
            setStatus(Status.MOVING_TO_OBJECT);
            update(0);
//        }
//        catch (NullPointerException e){
//            e.printStackTrace();
//        }
    }

    public void setMoveTargetPoint(float targetX, float targetY){
        BrakeDistance = HeroConstants.EMPTY_POINT_STOP_DISTANCE;
        setTargetCoords(targetX, targetY);
        setStatus(Status.MOVING_TO_EMPTY_POINT);
        update(0);
    }

    private void setTargetObject(GameObject pTargetObject){
        pTarget = pTargetObject;
        setTargetCoords(pTargetObject.getAbsX(), pTargetObject.getAbsY());
    }
    private void setTargetCoords(float targetX, float targetY){
        this.target_X = targetX;
        this.target_Y = targetY;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public byte update (float dt){

        switch (mCurrentStatus){
                case STAY: {

                    braking( HeroConstants.MIN_STAY_AND_MOVE_SPEED2, HeroConstants.HERO_BRAKING );
                    break;}

                case STAY_AND_MINE:{

                    this.targetACalculation();
                    this.targetDCalculation();

                    braking( HeroConstants.MIN_STAY_AND_MOVE_SPEED2, HeroConstants.HERO_BRAKING );

                    pTarget.getHealthObject().reduceHealth(500 * dt);
                    if (pTarget.getHealthObject().getHealth() <= 0) {
                        pTarget = null;
                        setStatus(Status.STAY);
                    }
                    break;
                }

                case ENTERING_BASE_AIMING:
                case STAY_AND_AIM:{

                    this.targetACalculation();
                    this.turnToTarget(true);
                    braking( HeroConstants.MIN_STAY_AND_MOVE_SPEED2, HeroConstants.HERO_BRAKING );
                            if (this.body.getAngularVelocity() == 0) {
                                if (mCurrentStatus == Status.STAY_AND_AIM)
                                    setStatus(Status.STAY_AND_MINE);
                                else
                                    setStatus(Status.ENTERING_BASE_MOVING);
                            }
                    break;
                }

                case LEAVING_BASE:
                case MOVING_TO_BASE:
                case ENTERING_BASE_MOVING:
                case MOVING_TO_OBJECT:
                case MOVING_TO_EMPTY_POINT: {

                    this.targetDCalculation();
                    if (target_D > BrakeDistance){
                        this.targetACalculation();
                        this.turnToTarget(true);

                        // prevent orbitum loop
                        if (Math.abs(MathUtils.sinDeg(mothership.getLocalCoordinates().getRotation() - target_A)) < HeroConstants.AIM_ANGLE_SIN_MAX_ON_THE_FLY
                                && MathUtils.cosDeg(mothership.getLocalCoordinates().getRotation() - target_A) > 0 ) {
                            this.body.applyLinearImpulse(
                                    -HeroConstants.HERO_ACCELERATION * MathUtils.sinDeg(target_A),
                                    HeroConstants.HERO_ACCELERATION * MathUtils.cosDeg(target_A),
                                     this.body.getPosition().x,
                                    this.body.getPosition().y,
                                    true);
                            float normal_bias = MathUtils.sinDeg(this.body.getLinearVelocity().angle() - target_A + 90 );
                            if (Math.abs(normal_bias) > AIM_ANGLE_SIN_MAX_ERROR) {
                                float tangent_bias = 1;
                                if (normal_bias > 0) tangent_bias *= -1;
                                this.body.applyLinearImpulse(
                                        HeroConstants.HERO_ACCELERATION * MathUtils.cosDeg(target_A) * tangent_bias,
                                        HeroConstants.HERO_ACCELERATION * MathUtils.sinDeg(target_A) * tangent_bias,
                                        this.body.getPosition().x,
                                        this.body.getPosition().y,
                                        true);
                            }

                        }
                        else
                            this.body.applyLinearImpulse(
                                    -this.body.getLinearVelocity().nor().x * HeroConstants.HERO_BRAKING,
                                    -this.body.getLinearVelocity().nor().y * HeroConstants.HERO_BRAKING,
                                    this.body.getPosition().x,
                                    this.body.getPosition().y, true);
                    }
                    else {
                        switch (mCurrentStatus){
                            case MOVING_TO_OBJECT: {
                                setStatus(Status.STAY_AND_AIM); break;}
                            case MOVING_TO_EMPTY_POINT: {
                                setStatus(Status.STAY); break;}
                            case MOVING_TO_BASE: {
                                setTargetObject(pTarget.getBaseProperty().getDockPoint());
                                setStatus(Status.ENTERING_BASE_AIMING); break;}
                            case ENTERING_BASE_MOVING:{
                                mothership.getHealthObject().getBody().setTransform(
                                        mothership.getHealthObject().getBody().getPosition(),
                                        mothership.getHealthObject().getBody().getAngle() + 180 * MathUtils.degreesToRadians);
                                setStatus(Status.LEAVING_BASE);
                                return 10;}
                            case LEAVING_BASE: {mCurrentStatus = Status.STAY; break;}
                        }
                    }
                }
            }

        if (this.body.getLinearVelocity().len2() > HeroConstants.HERO_MAX_SPEED)
            this.body.setLinearVelocity(this.body.getLinearVelocity().limit(HeroConstants.HERO_MAX_SPEED));

        this.mothership.getLocalCoordinates().setX(this.body.getPosition().x);
        this.mothership.getLocalCoordinates().setY(this.body.getPosition().y);
        this.mothership.getLocalCoordinates().setRotation(this.body.getAngle() * MathUtils.radiansToDegrees);

        setCurrentRotationFrame(false);
        return 0;
    }


    // ===========================================================
    // Methods
    // ===========================================================
    private void setCurrentRotationFrame(boolean changeState) {

        if(currentSprite.getPlayMode() == Animation.PlayMode.NORMAL
            && !currentSprite.isAnimationFinished())
                return;

        int rotationFrame = MathUtils.round(this.body.getAngle() / MathUtils.PI2 * 8
            + (mCurrentStatus == Status.STAY_AND_MINE ? -0.3f : 0 )
        );


        while (rotationFrame < 0)
            rotationFrame += 8;
        rotationFrame = rotationFrame % 8;

        if (!changeState && this.rotationFrame == rotationFrame)
            return;

        currentSprite.setEnabled(false);

        switch (mCurrentStatus) {
            case MOVING_TO_EMPTY_POINT:
            case MOVING_TO_OBJECT: {
                if (this.rotationFrame != rotationFrame || changeState) {
                    currentSprite = walkStateSprite[rotationFrame];
                    this.rotationFrame = rotationFrame;
                    currentSprite.setEnabled(true);
                }
                break;
            }
            case STAY_AND_MINE:{
                currentSprite = mineStateSprite[rotationFrame];
                this.rotationFrame = rotationFrame;
                currentSprite.resetLifeTime();
                currentSprite.setEnabled(true);
                break;
            }

            default:
            case STAY:
            case STAY_AND_AIM:{
//                if (this.rotationFrame != rotationFrame || changeState) {
                    currentSprite = stayStateSprite[rotationFrame];
                    this.rotationFrame = rotationFrame;
                    currentSprite.setEnabled(true);
//                }
                break;
            }
        }
    }

    public boolean isMoving(){
        switch (mCurrentStatus){
            case STAY:
            case STAY_AND_MINE:
            case STAY_AND_AIM: {return false;}
            default: return true;
        }
    }

    public boolean isBysy(){
        switch (mCurrentStatus){
            case ENTERING_BASE_AIMING:
            case ENTERING_BASE_MOVING:
            case LEAVING_BASE:
            case STAY_IN_BASE:
                { return true;}
            default: return false;
        }
    }

    /**
     * Braking method. Decrease hero speed every frame.\     *
     * @param squareOfMinSpeedBeforeSettingSpeedToZero When speed is below minSpeedBeforeSettingSpeedToZero
     *                                         - setting abs speed to 0
     * @param brakingValue abs value of braking impulse
     * @return true if object is still moving
     */
    private boolean braking(float squareOfMinSpeedBeforeSettingSpeedToZero, float brakingValue){
        if (body.getLinearVelocity().len2() > squareOfMinSpeedBeforeSettingSpeedToZero) {
            body.applyLinearImpulse(
                    -body.getLinearVelocity().nor().x * brakingValue,
                    -body.getLinearVelocity().nor().y * brakingValue,
                    body.getPosition().x,
                    body.getPosition().y, true);
            return true;
        }
        else
            body.setLinearVelocity(0,0);
        return false;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    private void targetACalculation(){
        target_A = - MathUtils.atan2(target_X - mothership.getAbsX(), target_Y - mothership.getAbsY())
                * MathUtils.radiansToDegrees;
    }

    private void targetDCalculation(){
        target_D = (float) Math.hypot((double) (mothership.getAbsX() - target_X), (double) (mothership.getAbsY() - target_Y));
    }

    private void turnToTarget() {this.turnToTarget(false);}
    private void turnToTarget(boolean directAiming){
        if ((Math.abs(MathUtils.sinDeg(mothership.getLocalCoordinates().getRotation() - target_A)) > AIM_ANGLE_SIN_MAX_ERROR || MathUtils.cosDeg(mothership.getLocalCoordinates().getRotation() - target_A) < 0 ))
        {
            if (Math.abs(this.body.getAngularVelocity()) < HeroConstants.HERO_MAX_ROTATION_SPEED) {
                if (MathUtils.sinDeg(mothership.getLocalCoordinates().getRotation() - target_A) > 0) {
                    this.body.applyAngularImpulse(-HeroConstants.HERO_RADIAL_ACCELERATION * MathUtils.radiansToDegrees, true);
                } else {
                    this.body.applyAngularImpulse(HeroConstants.HERO_RADIAL_ACCELERATION * MathUtils.radiansToDegrees, true);
                }
            }
        }
        else {
            stopAngularMovement();

            if (directAiming) {
                body.setTransform(body.getPosition(), MathUtils.degreesToRadians *target_A);
            }
        }
    }

    private void stopLinearMovement(){
        mothership.getHealthObject().getBody().setLinearVelocity(0,0);
    }

    private void stopAngularMovement(){
        this.body.setAngularVelocity(0);
    }

}
