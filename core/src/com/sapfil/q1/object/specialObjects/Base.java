package com.sapfil.q1.object.specialObjects;

import com.badlogic.gdx.physics.box2d.Body;
import com.sapfil.q1.object.GameObjects.GameObject;

/********************************
 * Created by Sapfil 
 * Edit 11.06.2017.
 * Notes
 ********************************/

public class Base {

    private com.sapfil.q1.object.GameObjects.GameObject pTarget = null;
    private com.sapfil.q1.object.GameObjects.GameObject mothership, entrancePoint, dockPoint;
    private Body body;

    public Base(com.sapfil.q1.object.GameObjects.GameObject mothership) {
        this.mothership = mothership;
        this.body = mothership.getHealthObject().getBody();
    }

    public void setEntrancePosint(com.sapfil.q1.object.GameObjects.GameObject entrancePosint){this.entrancePoint = entrancePosint;}


    public com.sapfil.q1.object.GameObjects.GameObject getDockPoint() {
        return dockPoint;
    }

    public void setDockPoint(com.sapfil.q1.object.GameObjects.GameObject dockPoint) {
        this.dockPoint = dockPoint;
    }

    public GameObject getEntrancepoint() {

        return entrancePoint;
    }

}
