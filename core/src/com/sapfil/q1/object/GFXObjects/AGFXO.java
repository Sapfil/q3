package com.sapfil.q1.object.GFXObjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.XML.DataBaseFromXML;

import static com.sapfil.q1.GlobalConstants.GFX_TO_PHYS;

/************************************************
 * Created by Sapfil on 15.09.2016.
 * Prev edit: 02-10-2016
 * Last edit: 18-10-16 Incapsulation, mess-cleaning
 * Last edit: 18-02-17 Blasts creation edit
 * Last edit: 30-08-17 converting to abstract
 * Notes: Memory-loss check
 ***********************************************/

public abstract class AGFXO extends Animation implements Comparable<AGFXO> {
    // ===========================================================
    // Constants
    // ===========================================================

    enum State {LOOP, NORMAL_PLAYING, NORMAL_ENDED}

    // ===========================================================
    // Fields
    // ===========================================================
    public static DataBaseFromXML sDB;
    public static float screenLeftBorder, screenRightBorder, screenUpBorder, screenDownBorder;

    protected float gfxCenterX, gfxCenterY;
    protected float regionWidth, regionHeight;
    private State state;
    private float z;

    protected float lifeTime = 0.0f;
    protected int fps;
    private boolean enabled = true;
    protected boolean isOnScreen = true;

    protected GameObject motherObject;
    private  Object[] keyFrames;
    protected boolean pseudoRotate = false;

    // ===========================================================
    // Constructors
    // ===========================================================

    protected AGFXO(int id){
        super (sDB.basicPartMap.get(id).duration, sDB.basicPartMap.get(id).textureRegion);
        this.fps = sDB.basicPartMap.get(id).fps;
        this.regionWidth= sDB.basicPartMap.get(id).textureRegion[0].getRegionWidth();
        this.regionHeight = sDB.basicPartMap.get(id).textureRegion[0].getRegionHeight();
        this.gfxCenterX = this.regionWidth/2;
        this.gfxCenterY= this.regionHeight/2;

        this.keyFrames = this.getKeyFrames();

//        if (id == 35)
//            System.out.println();

        state = State.LOOP;
        this.setPlayMode(PlayMode.REVERSED);
        if ( sDB.basicPartMap.get(id).fps != 0){
            this.setPlayMode(PlayMode.LOOP);
            if (sDB.basicPartMap.get(id).type_of_object == 900  //blast
                || sDB.basicPartMap.get(id).type_of_object == 2 ){     // hero - normal play
                    this.setPlayMode(PlayMode.NORMAL);
                    state = State.NORMAL_PLAYING;
            }
            if ( sDB.basicPartMap.get(id).fps < 0 )
                this.setFrameDuration(this.getAnimationDuration() * MathUtils.random(0.75f, 1.5f));
            }
        }

    public void setFrame(int frameNumber){
        this.setKeyFrames(keyFrames[frameNumber]);
    }
    public void setPseudoRotate(boolean pseudoRotate){
        this.pseudoRotate = pseudoRotate;
    }

    public float getGfxCenterX() { return gfxCenterX; }
    public float getGfxCenterY() { return gfxCenterY; }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setCoordinatesHolder(GameObject motherObject) {
        this.motherObject = motherObject;
    }

    public float getRegionWidth() {
        return regionWidth;
    }

    public float getRegionHeight() {
        return regionHeight;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces U/R/D
    // ===========================================================

    public void resetLifeTime(){
        lifeTime = 0.0f;
    }

    public boolean isAnimationFinished(){
        return isAnimationFinished(lifeTime);
    }

    public byte update(float dt){
        lifeTime += dt;
        if (this.getPlayMode() == PlayMode.NORMAL && this.isAnimationFinished(lifeTime)) {
            if (motherObject.getTypeOfObject() == 900)
                motherObject.dispose(false); // blast-object dispose

        }
        return 0;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float pZ) {
        z = pZ;
    }

    // !!!  needs begin/end for pSB in calling class
    public abstract void render (SpriteBatch pSB);
    public abstract void isOnScreen();

    public void dispose(){

    }

    @Override
    public int compareTo(AGFXO other) {
        if (this.z == other.getZ())
            return 0;
        else
        if (this.z > other.getZ())
            return 1;
        else return  -1;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public static class GfxObjectBuilder{

        private static AGFXO gfxObject;

        public GfxObjectBuilder createStaticGFxObjectEntity(int id, GameObject motherObject){
            if (motherObject.getHealthObject() == null) {
                    gfxObject = new StaticAnimatedObjectWithoutHealthObject(id, motherObject);
            }
            else{
                    gfxObject = new StaticAnimatedObjectWithHealthObject(id, motherObject);
            }
            gfxObject.setCoordinatesHolder(motherObject);
            return this;}

        public GfxObjectBuilder createGFxObjectEntity(int id, GameObject motherObject){

            if (motherObject.getHealthObject() == null)
                gfxObject = new AnimatedObjectWithoutHealthObject(id);
            else  if ( motherObject.getDependencyObject() == null)
                gfxObject = new AnimatedObjectWithHealthObject(id);
            else
                gfxObject = new AnimatedObjectWithDependency(id);
            gfxObject.setCoordinatesHolder(motherObject);

            return this;}

        public GfxObjectBuilder gfxDisable() {gfxObject.setEnabled(false); return this;}
        public GfxObjectBuilder gfxZcoord(float pZ)         {gfxObject.setZ(pZ); return this;}
        public AGFXO build() {return gfxObject;}

    }
}

abstract class StaticAGFXO extends AGFXO{

    protected float width, height;
    protected float rightSide, upSide;
    protected float x, y;

    protected StaticAGFXO(int id) {
        super(id);
    }

    protected void setOtherStaticParameters(){
        width = ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionWidth() * GFX_TO_PHYS;
        height = ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionHeight() * GFX_TO_PHYS;
        rightSide = x + width;
        upSide = y + height;
    }

    public void isOnScreen(){
        if(x > screenRightBorder) {
            isOnScreen = false;
            return;
        }
        if(rightSide < screenLeftBorder) {
            isOnScreen = false;
            return;
        }
        if(y > screenUpBorder) {
            isOnScreen = false;
            return;
        }
        if (upSide < screenDownBorder) {
            isOnScreen = false;
            return;
        }
        isOnScreen = true;
    }

    @Override
    public void render(SpriteBatch pSB){
        if (isOnScreen)
            pSB.draw((TextureRegion) this.getKeyFrame(this.lifeTime),
                   x, y, width, height);
    }
}

class StaticAnimatedObjectWithoutHealthObject extends StaticAGFXO {

    protected StaticAnimatedObjectWithoutHealthObject(int id, GameObject motherObject) {
        super(id);
        x = motherObject.getAbsX() - motherObject.getEntity().getOriginX();
        y = motherObject.getAbsY() - motherObject.getEntity().getOriginY();
        setOtherStaticParameters();
    }
}

class StaticAnimatedObjectWithHealthObject extends StaticAGFXO{

    protected StaticAnimatedObjectWithHealthObject(int id,  GameObject motherObject) {
        super(id);
        x = motherObject.getHealthObject().getBody().getPosition().x - motherObject.getEntity().getOriginX();
        y = motherObject.getHealthObject().getBody().getPosition().y - motherObject.getEntity().getOriginY();
        setOtherStaticParameters();
    }
}

class AnimatedObjectWithoutHealthObject extends AGFXO {

    private float width, height;

    public AnimatedObjectWithoutHealthObject(int id) {
        super(id);
        width = ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionWidth() * GFX_TO_PHYS;
        height = ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionHeight() * GFX_TO_PHYS;
    }

    public void isOnScreen(){
        float leftSide = motherObject.getAbsX() - motherObject.getEntity().getOriginX();
        if(leftSide > screenRightBorder) {
            isOnScreen = false;
            return;
        }
        float rightSide = leftSide + width;
        if(rightSide < screenLeftBorder) {
            isOnScreen = false;
            return;
        }
        float downSide = motherObject.getAbsY() - motherObject.getEntity().getOriginY();
        if(downSide > screenUpBorder) {
            isOnScreen = false;
            return;
        }
        float upSide = downSide + height;
        if (upSide < screenDownBorder) {
            isOnScreen = false;
            return;
        }
        isOnScreen = true;
    }

    @Override
    public void render(SpriteBatch pSB){
        if (isOnScreen)
            pSB.draw((TextureRegion) this.getKeyFrame(this.lifeTime),                   //texture region
                    motherObject.getAbsX() - motherObject.getEntity().getOriginX(),
                    motherObject.getAbsY() - motherObject.getEntity().getOriginY(),
                    motherObject.getEntity().getOriginX(), motherObject.getEntity().getOriginY(),   //deform-center
                    width,              //width
                    height,             //height
                    motherObject.getEntity().getScaleX(), motherObject.getEntity().getScaleY(),     //stretching deform (default = 1)
                    motherObject.getLocalCoordinates().getRotation() + motherObject.getCoordinatesCorrection().getRotation());
    }
}

class AnimatedObjectWithHealthObject extends AGFXO {

    private float width, height;

    public AnimatedObjectWithHealthObject(int id) {
        super(id);
        width = ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionWidth() * GFX_TO_PHYS;
        height = ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionHeight() * GFX_TO_PHYS;
    }

    public void isOnScreen(){
        float leftSide = motherObject.getHealthObject().getBody().getPosition().x - motherObject.getEntity().getOriginX();
        if(leftSide > screenRightBorder) {
            isOnScreen = false;
            return;
        }
        float rightSide = leftSide + width;
        if(rightSide < screenLeftBorder) {
            isOnScreen = false;
            return;
        }
        float downSide = motherObject.getHealthObject().getBody().getPosition().y - motherObject.getEntity().getOriginY();
        if(downSide > screenUpBorder) {
            isOnScreen = false;
            return;
        }
        float upSide = downSide + height;
        if (upSide < screenDownBorder) {
            isOnScreen = false;
            return;
        }
        isOnScreen = true;
    }

    @Override
    public void render(SpriteBatch pSB){

            if (isOnScreen)
                pSB.draw((TextureRegion) this.getKeyFrame(this.lifeTime),                   //texture region
                        motherObject.getHealthObject().getBody().getPosition().x - motherObject.getEntity().getOriginX(),
                        motherObject.getHealthObject().getBody().getPosition().y - motherObject.getEntity().getOriginY(),
                        motherObject.getEntity().getOriginX(), motherObject.getEntity().getOriginY(),   //deform-center
                        width,              //width
                        height,             //height
                        motherObject.getEntity().getScaleX(), motherObject.getEntity().getScaleY(),     //stretching deform (default = 1)
                        (pseudoRotate ? 0 : motherObject.getHealthObject().getBody().getAngle() * MathUtils.radiansToDegrees));
//                    motherObject.getHealthObject().getBody().getAngle() * MathUtils.radiansToDegrees)

    }
}

class AnimatedObjectWithDependency extends AGFXO {

    public AnimatedObjectWithDependency(int id) {
        super(id);
    }

    @Override
    public void render(SpriteBatch pSB){
        pSB.draw( (TextureRegion)this.getKeyFrame(this.lifeTime),                   //texture region
                motherObject.getAbsX() - motherObject.getEntity().getOriginX(),
                motherObject.getAbsY() - motherObject.getEntity().getOriginY(),
                motherObject.getEntity().getOriginX(), motherObject.getEntity().getOriginY(),   //deform-center
                ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionWidth() * GFX_TO_PHYS,              //width
                ((TextureRegion)this.getKeyFrame(this.lifeTime)).getRegionHeight() * GFX_TO_PHYS,             //height
                motherObject.getEntity().getScaleX(), motherObject.getEntity().getScaleY(),     //stretching deform (default = 1)
               motherObject.getHealthObject().getBody().getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public void isOnScreen() {
        isOnScreen = true;
    }
}