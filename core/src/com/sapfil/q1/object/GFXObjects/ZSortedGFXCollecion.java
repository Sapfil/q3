package com.sapfil.q1.object.GFXObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.sapfil.q1.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 20.10.2016.
 * Last edit:
 * Notes:
 ***********************************************/
public class ZSortedGFXCollecion {
    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    protected Array<AGFXO> objectsArray;
    private boolean isSorted = true;
    private boolean isFull = true;      // no null-objects in list

    private byte onScreenCheckCounter;
    private final byte onScreenCheckCounterMax = 10;
    private int batchStartIndex = -1, batchEndIndex;

    // ===========================================================
    // Constructors
    // ===========================================================

    public ZSortedGFXCollecion() {
        Gdx.app.log("Sapfil_Log", "New LocalPositionCollection created");
        objectsArray = new Array<>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getTotalObjects() {
        return (objectsArray.size);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt) {

        for (int i = 0; i < objectsArray.size; i++) {
            objectsArray.get(i).update(dt);
            if (i % onScreenCheckCounterMax == onScreenCheckCounter)
                objectsArray.get(i).isOnScreen();
        }

            onScreenCheckCounter++;
            if (onScreenCheckCounter == onScreenCheckCounterMax)
                onScreenCheckCounter = 0;

        if (!isSorted) {
            objectsArray.sort();
            isSorted = true;
        }

        return 0;
    }

    public void render(SpriteBatch pSB) {

        pSB.begin();
        for (AGFXO nextSprite : objectsArray)
                if (nextSprite.isEnabled())
                    nextSprite.render(pSB);

        pSB.end();
    }

    public void dispose() {
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject(AGFXO basicObject){

        if (objectsArray.size > 0)
            if (objectsArray.get(objectsArray.size -1).getZ() > basicObject.getZ())
                isSorted = false;
        objectsArray.add(basicObject);
    }

    /**
     * ОБЯЗАТЕЛЕН конечный вызов deleteWholeHex (не батчевой)
     * Формирует батч на удаление.
     * Изначально стартовый индекс батча = -1, значит батча еще нет.
     * Первый объект инициализирует номер начала и конца батча своим номером.
     * Последующие объекты проверяются - если они в начале или конце батча - батч расширяется.
     * Если новый объект не рядом с батчем, то текущий батч удаляется и начинается формирование нового.
     * @param sprite
     */
    public void deletObjectUsingBatch(AGFXO sprite) {
        sprite.dispose();

        if (batchStartIndex == -1){
            batchStartIndex = objectsArray.indexOf(sprite, true);
            batchEndIndex = batchStartIndex;
        }
        else
            if ((batchEndIndex != objectsArray.size - 1) && objectsArray.get(batchEndIndex + 1) == sprite)
                batchEndIndex++;
            else if ((batchStartIndex > 0 ) && objectsArray.get(batchStartIndex - 1) == sprite)
                batchStartIndex--;
            else
            {
                objectsArray.removeRange(batchStartIndex, batchEndIndex);
                batchStartIndex = objectsArray.indexOf(sprite, true);
                batchEndIndex = batchStartIndex;
            }
    }

    public void deleteObject(AGFXO sprite) {
        sprite.dispose();
        if (batchStartIndex != -1) {
            int i = objectsArray.indexOf(sprite, true);
            if (i < batchStartIndex || i > batchEndIndex) {
                objectsArray.removeRange(batchStartIndex, batchEndIndex);
                objectsArray.removeValue(sprite, true);
            } else {
                objectsArray.removeRange(batchStartIndex, batchEndIndex);
            }
            batchStartIndex = -1;
        }
        else
            objectsArray.removeValue(sprite, true);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}