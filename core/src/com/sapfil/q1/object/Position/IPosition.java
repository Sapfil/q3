package com.sapfil.q1.object.Position;

import com.sapfil.q1.object.GameObjects.GameObject;

/************************************************
 * Created by Sapfil on 25.02.2017.
 * Last edit: 19.03.2017 working prototype
 * Edit: 08-05-2017. ID and TOO - deprecated
 * Notes: started from 15th project version
 ***********************************************/
public interface IPosition extends Comparable<IPosition>{

    // ===========================================================
    // Constants
    // ===========================================================
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    byte update(float dt);

    // Center is center-point in ABS coords (example: for direct-aiming)
    // Origin is local center for deformation
//    float getX();           float getY();           float getRotation();
//    void  setX(float pX);   void  setY(float pY);   void setRotation(float pRotation);

    // Origin is entityRotation and deform center.
    float getOriginX();     float getOriginY();
    void setOriginXY(float pX, float pY);

    //z-coords
//    float getZ();           void setZ(float pZ);

    // Speed V bp absolute speed
    float getSpeedX();      float getSpeedY();
    float getSpeedV();
    float getRotationSpeed();

    void setSpeedXY(float pVX, float pVY);
    void setSpeedV(float pV);
    void setRotationSpeed(float pVR);

    void setScale(float pSX, float pSY);
    float getScaleX();
    float getScaleY();

    // all coordinates above are local coordinates relatively to parent
    // if (parent == null) then global coords == local coords
//    Coordinates calculateGlobalCoordinatesCorrection();

//    Coordinates getCoordinates();
//    void setCoordinates(Coordinates coordinates);
    GameObject getMothership();


}
