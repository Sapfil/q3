package com.sapfil.q1.object.Position;

import com.badlogic.gdx.math.MathUtils;

/************************************************
 * Created by Sapfil on 19.03.2017.
 * Last edit:
 * Notes:
 ***********************************************/
public class Coordinates {

    private float x,y,z,rotation;

    public Coordinates() {
    }

    public Coordinates(float x, float y, float z, float rotation) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.rotation = rotation;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public void copy(Coordinates coordinates){
        this.x = coordinates.getX();
        this.y = coordinates.getY();
        this.z = coordinates.getZ();
        this.rotation = coordinates.getRotation();
    }

    public void add(Coordinates coordinates){
        this.x += coordinates.getX();
        this.y += coordinates.getY();
        this.z += coordinates.getZ();
        this.rotation += coordinates.getRotation();
    }

    public void minus(Coordinates coordinates){
        this.x -= coordinates.getX();
        this.y -= coordinates.getY();
        this.z -= coordinates.getZ();
        this.rotation -= coordinates.getRotation();
    }

    public float Angle_Calculation(){
        return - MathUtils.atan2(x, y)
                * MathUtils.radiansToDegrees;
    }

    public float Distance_Calculation(){
        return (float) Math.hypot(x, y);
    }
}
