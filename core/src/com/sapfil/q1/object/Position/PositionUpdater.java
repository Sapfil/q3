package com.sapfil.q1.object.Position;

import com.badlogic.gdx.math.MathUtils;
import com.sapfil.q1.object.GameObjects.GameObject;

/************************************************
 * Created by Sapfil on 26.02.2017.
 * Last edit: 19.03.2017 working prototype
 * Notes: started from 15th project version
 ***********************************************/
public class PositionUpdater implements com.sapfil.q1.object.Position.IPosition {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // "All Coordinates" field group
    private float origin_x, origin_y, scaleX = 1.0f, scaleY = 1.0f;
    private float vX, vY, vABS, rotationSpeed;

    // "Game logic basic parameters" fields group
    private int typeOfObject, id;
//    private Coordinates coordinates;
    private GameObject mothership;

    // "Parents and children" fields group


    // ===========================================================
    // Constructors
    // ===========================================================

    // Using private constructor and inner Builder (see below)
    public PositionUpdater(GameObject mothership){
        this.mothership = mothership;
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

//    public float getX() {           return mothership.getLocalCoordinates().getX();}
//
//    public float getY() {           return mothership.getLocalCoordinates().getY();}
//
//    public float getZ() {           return mothership.getLocalCoordinates().getZ();}
//
//    public float getRotation() {    return mothership.getLocalCoordinates().getRotation();}
//
//
//    public void setX(float pX) {    this.mothership.getLocalCoordinates().setX(pX);}
//
//    public void setY(float pY) {    this.mothership.getLocalCoordinates().setY(pY);}
//
//    public void setZ(float pZ) {    this.mothership.getLocalCoordinates().setZ(pZ);}
//
//    public void setRotation(float pA){this.mothership.getLocalCoordinates().setRotation(pA);}

    @Override
    public void setOriginXY(float pX, float pY) {
        this.origin_x = pX;
        this.origin_y = pY;
    }
    @Override
    public float getOriginX() {     return origin_x;      }
    @Override
    public float getOriginY() {     return origin_y;      }

    @Override
    public void setScale(float pSX, float pSY) {
        this.scaleX = pSX;
        this.scaleY = pSY;
    }
    @Override
    public float getScaleX() {
        return scaleX;
    }
    @Override
    public float getScaleY() {
        return scaleY;
    }

    @Override
    public GameObject getMothership() {
        return mothership;
    }

    @Override
    public void setSpeedXY(float pVX, float pVY) {
        this.vX = pVX;
        this.vY = pVY;
        this.vABS = (float) Math.hypot((double) (-vX), (double) (-vY));
    }
    @Override
    public void setSpeedV(float pV) {
        vABS = pV;
        vX = -pV * (MathUtils.sinDeg(mothership.getLocalCoordinates().getRotation()));
        vY = pV * (MathUtils.cosDeg(mothership.getLocalCoordinates().getRotation()));
    }
    @Override
    public float getSpeedX() {
        return vX;
    }
    @Override
    public float getSpeedY() {
        return vY;
    }
    @Override
    public float getSpeedV() {
        return vABS;
    }
    @Override
    public float getRotationSpeed() {
        return rotationSpeed;
    }
    @Override
    public void setRotationSpeed(float pVR) {
        this.rotationSpeed = pVR;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    @Override
    public byte update(float dt) {

        // updating LOCAL coordinates
        mothership.getLocalCoordinates().setRotation(mothership.getLocalCoordinates().getRotation() + rotationSpeed * dt);

        if( this.getSpeedV() != 0){
            if (this.getRotationSpeed() != 0) {
                this.setSpeedV(this.getSpeedV());
            }
            mothership.getLocalCoordinates().setX(mothership.getLocalCoordinates().getX() + vX * dt);
            mothership.getLocalCoordinates().setY(mothership.getLocalCoordinates().getY() + vY * dt);
        }
        return 0;
    }

    @Override
    public int compareTo(IPosition other) {
        if (this.mothership.getLocalCoordinates().getZ() == other.getMothership().getLocalCoordinates().getZ())
            return 0;
        else
        if (this.mothership.getLocalCoordinates().getZ() > other.getMothership().getLocalCoordinates().getZ())
            return 1;
        else return  -1;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public static class EntityBuilder{

        private static PositionUpdater positionUpdater;

        public EntityBuilder createEntity(GameObject mothership){
            positionUpdater = new PositionUpdater(mothership);
            return this;}

        public EntityBuilder entityScale(float sX, float sY){
            positionUpdater.setScale(sX, sY); return this;}
        public EntityBuilder entitySpeedXY(float pVX, float pVY){
            positionUpdater.setSpeedXY(pVX, pVY); return this;}
        public EntityBuilder entitySpeedV(float pV){
            positionUpdater.setSpeedV(pV); return this;}
        public EntityBuilder entityRotationSpeed(float pRotaionSpeed){
            positionUpdater.setRotationSpeed(pRotaionSpeed); return this;}
        public EntityBuilder entityOriginXY(float originX, float originY){
            positionUpdater.setOriginXY(originX, originY); return this;}
        public PositionUpdater build() {return positionUpdater;}

    }
}
