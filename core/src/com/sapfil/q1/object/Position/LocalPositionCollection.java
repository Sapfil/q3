package com.sapfil.q1.object.Position;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.sapfil.q1.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 20.10.2016.
 * Last edit:
 * Notes:
 ***********************************************/
public class LocalPositionCollection {
    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    protected Array<com.sapfil.q1.object.Position.IPosition> objectsArray;

    // ===========================================================
    // Constructors
    // ===========================================================

    public LocalPositionCollection() {
        Gdx.app.log("Sapfil_Log", "New LocalPositionCollection created");
        objectsArray = new Array<com.sapfil.q1.object.Position.IPosition>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getTotalObjects() {
        return (objectsArray.size);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt) {

        for (int i = 0; i < objectsArray.size; i++)
            objectsArray.get(i).update(dt);
        return 0;
    }

    public void dispose() {
        objectsArray = null;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void register(com.sapfil.q1.object.Position.IPosition entity){
        objectsArray.add(entity);
    }

    public void unregister(IPosition entity) {
        objectsArray.removeValue(entity,true);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}