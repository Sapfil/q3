package com.sapfil.q1.object.ObjectDependency;

import com.sapfil.q1.object.GameObjects.GameObject;

import java.util.List;

/************************************************
 * Created by Sapfil on 25.03.2017.
 * Last edit:
 * Notes:
 ***********************************************/
public interface IDependency {
    // "parents and children" methods group
    void attachChild(GameObject child);
    void detachChild(GameObject child);
    void detachFirst();
    void detachLast();
    void detachAll();
    void detachSelf();
    GameObject getFirstChild();
    GameObject getLastChild();
    List<GameObject> getChildren();
    GameObject getParent();

//    IPosition getLocalPosition();
    void calculateGlobalCoordinatesCorrection();

    public GameObject getMotherObject() ;
    public void setMotherObject(GameObject motherObject);
}
