package com.sapfil.q1.object.ObjectDependency;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.sapfil.q1.XML.DataBaseFromXML;

/************************************************
 * Created by Sapfil on 25.03.2017.
 * Last edit:
 * Notes:
 ***********************************************/
public class ChildrenCollection {


    // ===========================================================
    // Constants and static fields
    // ===========================================================

    public static DataBaseFromXML sDB;

    // ===========================================================
    // Fields
    // ===========================================================

    protected Array<IDependency> objectsArray;

    // ===========================================================
    // Constructors
    // ===========================================================

    public ChildrenCollection() {
        Gdx.app.log("Sapfil_Log", "New ChildrenCollection created");
        objectsArray = new Array<IDependency>();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getTotalObjects() {
        return (objectsArray.size);
    }

    public IDependency getObjectByIndex(int i) {
        return this.objectsArray.get(i);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces (U/R/D)
    // ===========================================================

    public byte update(float dt) {

        for (int i = 0; i < objectsArray.size; i++)
            objectsArray.get(i).calculateGlobalCoordinatesCorrection();
        return 0;

    }

    public void dispose(){
        for (IDependency dependency : objectsArray)
            dependency.detachAll();
        objectsArray.clear();
    }


    // ===========================================================
    // Methods
    // ===========================================================

    public void addNewObject(IDependency positionDependency){
        this.objectsArray.add(positionDependency);
    }

    public void deleteObject(IDependency positionDependency) {
        objectsArray.removeValue(positionDependency, true);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
