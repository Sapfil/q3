package com.sapfil.q1.object.ObjectDependency;

import com.badlogic.gdx.math.MathUtils;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.object.Position.Coordinates;

import java.util.LinkedList;
import java.util.List;

/************************************************
 * Created by Sapfil on 25.03.2017.
 *
 * Last edit:
 * Notes:
 ***********************************************/
public class PositionDependency implements IDependency {

    private List<GameObject> children = new LinkedList<>();
//    private IDependency parent = null;
    private Coordinates globalCoordinatesCorrection;
    private Coordinates localCoordinates;

    private GameObject motherObject;
    private GameObject parentObject;

    public PositionDependency(GameObject parentObject, GameObject motherObject){
        this.motherObject = motherObject;
        this.parentObject = parentObject;
        globalCoordinatesCorrection = new Coordinates();
        localCoordinates = new Coordinates();
        if (parentObject != null)
            calculateGlobalCoordinatesCorrection();
    }

    @Override
    public void attachChild(GameObject child) {        children.add(child);    }
    @Override
    public void detachChild(GameObject child) {        children.remove(child);    }
    @Override
    public void detachFirst() {
        if (children.size() > 0)
            children.remove(0);
    }
    @Override
    public void detachLast() {
        if (children.size() > 0)
            children.remove(children.size()-1);
    }
    @Override
    public void detachAll(){
        for (GameObject child : children)
            child.dispose();
        children = new LinkedList<>();
    }
    @Override
    public void detachSelf(){        this.getParent().getDependencyObject().detachChild(motherObject);    }
    @Override
    public GameObject getFirstChild() {
        if (children.size() > 0)
            return children.get(0);
        return null;
    }
    @Override
    public GameObject getLastChild() {
        if (children.size() > 0)
            return children.get(children.size()-1);
        return null;
    }

    @Override
    public List<GameObject> getChildren() {
        return children;
    }
    @Override
    public GameObject getParent() {
        return parentObject;
    }

//    @Override
//    public IPosition getLocalPosition(){
//        return localPosition;
//    }

    @Override
    public GameObject getMotherObject() {
        return motherObject;
    }

    @Override
    public void setMotherObject(GameObject motherObject) {
        this.motherObject = motherObject;
    }


    public void calculateGlobalCoordinatesCorrection(){

        // father object is diposed
        if(parentObject.getLocalCoordinates() == null){
            motherObject.dispose(true);
            return;
        }

        // calculating parent global coordinates
        globalCoordinatesCorrection.copy(parentObject.getLocalCoordinates());
        globalCoordinatesCorrection.add(parentObject.getCoordinatesCorrection());

        //calculating local distance and angle

        localCoordinates.copy(motherObject.getLocalCoordinates());
        float localDistanceFromParent = localCoordinates.Distance_Calculation();
        float localAngleFromParent = localCoordinates.Angle_Calculation();

        if (parentObject.getHealthObject() != null)
            if (parentObject.getDependencyObject() == null) {
                parentObject.getLocalCoordinates().setX(parentObject.getHealthObject().getBody().getPosition().x);
                parentObject.getLocalCoordinates().setY(parentObject.getHealthObject().getBody().getPosition().y);
                parentObject.getLocalCoordinates().setRotation(parentObject.getHealthObject().getBody().getAngle() * MathUtils.radiansToDegrees);
            }


        //calculating correction using parent globalCoords and local distance and angle
        globalCoordinatesCorrection.setX(globalCoordinatesCorrection.getX() - motherObject.getLocalCoordinates().getX()
                - localDistanceFromParent * MathUtils.sinDeg(localAngleFromParent + parentObject.getLocalCoordinates().getRotation() + parentObject.getCoordinatesCorrection().getRotation()));
        globalCoordinatesCorrection.setY(globalCoordinatesCorrection.getY() - motherObject.getLocalCoordinates().getY()
                + localDistanceFromParent * MathUtils.cosDeg(localAngleFromParent + parentObject.getLocalCoordinates().getRotation() + parentObject.getCoordinatesCorrection().getRotation()));

        //setting result to mothership
        motherObject.setCoordinatesCorrection(globalCoordinatesCorrection);
    }


}
