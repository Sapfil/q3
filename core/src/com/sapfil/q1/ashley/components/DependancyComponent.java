package com.sapfil.q1.ashley.components;

import com.badlogic.ashley.core.Component;
import com.sapfil.q1.object.GameObjects.GameObject;

import java.util.List;

/**
 * Created by Sapfil on 08.10.2017.
 */

public class DependancyComponent implements Component {

    private List<GameObject> children = null;

    private GameObject motherObject;
    private GameObject parentObject;

    public DependancyComponent(GameObject parentObject, GameObject motherObject){
        this.motherObject = motherObject;
        this.parentObject = parentObject;
    }

    public void attachChild(GameObject child) {        children.add(child);    }
    public void detachChild(GameObject child) {        children.remove(child);    }
    public void detachFirst() {
        if (children.size() > 0)
            children.remove(0);
    }
    public void detachLast() {
        if (children.size() > 0)
            children.remove(children.size()-1);
    }
    public void detachAll(){        children = null;    }
    public void detachSelf(){       this.getParent().getDependencyObject().detachChild(motherObject);    }

    public GameObject getFirstChild() {
        if (children.size() > 0)
            return children.get(0);
        return null;
    }
    public GameObject getLastChild() {
        if (children.size() > 0)
            return children.get(children.size()-1);
        return null;
    }

    public List<GameObject> getChildren() {
        return null;
    }
    public GameObject getParent() {
        return parentObject;
    }



    public GameObject getMotherObject() {
        return motherObject;
    }

}
