package com.sapfil.q1.ashley.mappers;

import com.badlogic.ashley.core.ComponentMapper;
import com.sapfil.q1.ashley.components.DeformComponent;
import com.sapfil.q1.ashley.components.DependancyComponent;
import com.sapfil.q1.ashley.components.PositionComponent;
import com.sapfil.q1.ashley.components.VelocityComponent;
import com.sapfil.q1.ashley.components.ZComponent;

/**
 * Created by Sapfil on 08.10.2017.
 */

public class Mappers {

    public static final ComponentMapper<PositionComponent> positionMapper
            = ComponentMapper.getFor(PositionComponent.class);

    public static final ComponentMapper<DeformComponent> deformMapper
            = ComponentMapper.getFor(DeformComponent.class);

    public static final ComponentMapper<DependancyComponent> depandancyMapper
            = ComponentMapper.getFor(DependancyComponent.class);

    public static final ComponentMapper<VelocityComponent> velocityMapper
            = ComponentMapper.getFor(VelocityComponent.class);

    public static final ComponentMapper<ZComponent> zMapper
            = ComponentMapper.getFor(com.sapfil.q1.ashley.components.ZComponent.class);
}
