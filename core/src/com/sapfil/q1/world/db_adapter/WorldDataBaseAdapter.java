package com.sapfil.q1.world.db_adapter;

import com.sapfil.q1.db.api.DAO;
import com.sapfil.q1.db.api.DataBase;
import com.sapfil.q1.db.level.LevelDB;
import com.sapfil.q1.db.level.dao.CellDTO;
import com.sapfil.q1.db.level.key.CellKey;
import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.world.cell.WorldCell;

import java.util.HashSet;


/**
 * Текущая реализация обращается напрямую в БД - JAVA-отображение XML файла
 * Вероятно нужно стремиться к тому чтобы развязать этот момент.
 */
public class WorldDataBaseAdapter {

    private final DataBase levelDataBase;
    private final DAO<CellKey, CellDTO> cellDAO;

    public WorldDataBaseAdapter(String worldName) {
        this.levelDataBase = new LevelDB(worldName);
        levelDataBase.getTableNames();
        cellDAO = levelDataBase.getDataBaseTable("cells");
    }

    public WorldCell getWorldCell(CellKey cellKey,
                                  Layer mainLayer,
                                  Layer debugLayer) {

            CellDTO dto = cellDAO.read(cellKey);
            if (dto != null) {
                return new WorldCell(
                        dto,
                        mainLayer,
                        debugLayer);
            } else
                return new WorldCell(
                        new CellDTO(
                                cellKey.x,
                                cellKey.y,
                                new HashSet<>()
                        ),
                        mainLayer,
                        debugLayer
                );
    }

    public void updateWorldCell(WorldCell updatedCell) {
        cellDAO.update(updatedCell.getKey(), updatedCell.getDTO());
    }

    public void dumpLevel() {
        levelDataBase.saveDataBase();
    }
}
