package com.sapfil.q1.world.cell;

import com.badlogic.gdx.math.Vector2;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.db.level.dao.CellDTO;
import com.sapfil.q1.db.level.dao.CellObjectDTO;
import com.sapfil.q1.db.level.key.CellKey;
import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.object.GameObjects.GameObject;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import platform.Disposable;


public class WorldCell implements Disposable {

    private boolean isMutable = false;

    private final GameObject techSprite;
    private final Vector2 centerCoordinates;

    private final Set<GameObject> gameObjectSet;

    public WorldCell(CellDTO dao,
                     Layer mainLayer,
                     Layer debugLayer) {

        this.centerCoordinates = new Vector2(dao.getX(), dao.getY());

        // технический спрайт
        techSprite = new GameObject.GameObjectBuilder()
                .create(60000, debugLayer)
                .withXYcoords(
                        dao.getX(),
                        dao.getY())
                .withZcoord(-999)
                .withScale(GlobalConstants.CELL_PHYS_SIZE_X / (128 * GlobalConstants.GFX_TO_PHYS),
                        GlobalConstants.CELL_PHYS_SIZE_Y / (128 * GlobalConstants.GFX_TO_PHYS))
                .build();

        // игровые объекты
        gameObjectSet = new HashSet<>();

        for (CellObjectDTO cellObjectDto : dao.getCellObjectSet()) {
            gameObjectSet.add(
                    new GameObject.GameObjectBuilder()
                            .create(cellObjectDto.id, mainLayer)
                            .withXYcoords(
                                    cellObjectDto.x,
                                    cellObjectDto.y,
                                    true)
                            .withZcoord(cellObjectDto.z)
                            .addDisposeConsumer((obj) -> {gameObjectSet.remove(obj); this.setMutable();})
                            .build()
            );
        }
    }

    public CellDTO getDTO() {
        return new CellDTO(centerCoordinates.x, centerCoordinates.y,
                gameObjectSet.stream()
                        .map( o -> new CellObjectDTO(o.getID(), o.getAbsX(), o.getAbsY(), o.getAbsZ()))
                        .collect(Collectors.toSet()));
    }

    public void addGameObject(GameObject gameObject){
        setMutable();
        gameObjectSet.add(gameObject);
    }

    public boolean isMutable() {
        return isMutable;
    }

    public void setMutable() {
        isMutable = true;
    }

    public Vector2 getCenterCoordinates() {
        return centerCoordinates;
    }

    public CellKey getKey() {
        return new CellKey(centerCoordinates.x, centerCoordinates.y);
    }

    @Override
    public void dispose() {
        techSprite.dispose();
        Set<GameObject> cloneSet = new HashSet<>(gameObjectSet);
        cloneSet.forEach(GameObject::dispose);
    }
}
