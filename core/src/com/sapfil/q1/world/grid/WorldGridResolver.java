package com.sapfil.q1.world.grid;

import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.db.level.key.CellKey;
import com.sapfil.q1.state.State;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Вычисляет "натуральные" координаты ячеек мира, кототрые надо добаылять/удялять.
 * Под "натуральными" понимается номер ячейки относительно центра мира.
 * То есть координата (2,3) обозначает 2 ячейки налево от центральной, 3 ячейки вверх.
 *
 * Все внутреннире данные класса хранятся именно в описанном выше "натуральном" виде, но
 * возвращаемые методами значения приводятся к стандартным "физическим" значениям.
 *
 * Класс должен знать о текущих краях активной области. А так же о размерах ячейки.
 * Например, для 2д-мира - это края экрана.
 *
 */
public class WorldGridResolver {

    private SortedMap<Integer, LinkedList<Integer>> table;
    private final State state;

    private int leftBorder, rightBorder, upBorder, downBorder;

    public WorldGridResolver(State state) {
        this.state = state;
        table = new TreeMap<>();
    }

    public CellKey getCellKeyForCoords(float x, float y)  {
        return new CellKey(
                Math.round(x /  GlobalConstants.CELL_PHYS_SIZE_X) * GlobalConstants.CELL_PHYS_SIZE_X,
                Math.round(y /  GlobalConstants.CELL_PHYS_SIZE_Y) * GlobalConstants.CELL_PHYS_SIZE_Y
        );
    }

    /**
     * Первичная инициализация таблицы координат ячеек мира
     * @return набор пар координат, которые полностью или частично присутствуют на экране.
     */
    public Set<CellKey> getInitSet(){

        resolveScreenBorders();

        for (int x = leftBorder; x <= rightBorder; x++){
            table.put(x, new LinkedList<>());
            for (int y = downBorder; y <= upBorder; y++){
                table.get(x).add(y);
            }
        }

        Set<CellKey> returnSet = new HashSet<>();

        for (Map.Entry<Integer, LinkedList<Integer>> entry : table.entrySet()){
            for(int y : entry.getValue()){
                returnSet.add(new CellKey(
                        entry.getKey() * GlobalConstants.CELL_PHYS_SIZE_X,
                        y * GlobalConstants.CELL_PHYS_SIZE_Y
                ));
            }
        }

        return returnSet;
    }

    /**
     *  Обновить данные о границах видимой области.
     */
    public void update(){
        resolveScreenBorders();
    }

    /**
     * Проверить текущие границы экрана и вернуть координаты тех ячеек, которые
     * полностью не видны на экране.
     * @return координаты ячеек на удаление
     */
    public Set<CellKey> getDeleteSet(){

        Set<CellKey> returnSet = new HashSet<>();

        findAndDeleteColumns(returnSet);

        findAndDeleteRows(returnSet);

        return returnSet;
    }

    /**
     * Проверить текущие границы экрана и вернуть координаты, по которым должны быть ячейки,
     * но их пока нет.
     * @return координаты ячеек на добавление.
     */
    public Set<CellKey> getNewSet(){

        Set<CellKey> returnSet = new HashSet<>();

        findAndAddColumns(returnSet);

        findAndAddRows(returnSet);

        return returnSet;
    }

    private void resolveScreenBorders(){
        float x = state.focusSupplier.get().x;
        float y = state.focusSupplier.get().y;
        float width = state.widthSupplier.get();
        float height = state.heightSupplier.get();

        leftBorder = Math.round((x - width/2) / GlobalConstants.CELL_PHYS_SIZE_X);
        rightBorder = Math.round((x + width/2) / GlobalConstants.CELL_PHYS_SIZE_X);
        downBorder = Math.round((y - height/2) / GlobalConstants.CELL_PHYS_SIZE_Y);
        upBorder = Math.round((y + height/2) / GlobalConstants.CELL_PHYS_SIZE_Y);
    }



    private void findAndDeleteColumns(Set<CellKey> returnSet) {

        while (table.firstKey() < leftBorder){
            int x = table.firstKey();
            for (int y : table.remove(table.firstKey())){
                returnSet.add(new CellKey(
                        x * GlobalConstants.CELL_PHYS_SIZE_X,
                        y * GlobalConstants.CELL_PHYS_SIZE_Y
                ));
            }
        }

        while (table.lastKey() > rightBorder){
            int x = table.lastKey();
            for (int y : table.remove(table.lastKey())){
                returnSet.add(new CellKey(
                        x * GlobalConstants.CELL_PHYS_SIZE_X,
                        y * GlobalConstants.CELL_PHYS_SIZE_Y
                ));
            }
        }
    }

    private void findAndAddColumns(Set<CellKey> returnSet) {

        while (table.firstKey() > leftBorder){
            LinkedList<Integer> copiedList = table.get(table.firstKey());
            table.put(table.firstKey() - 1, new LinkedList(copiedList));
            for (int y = copiedList.getFirst(); y <= copiedList.getLast(); y++){
                returnSet.add(new CellKey(
                        table.firstKey() * GlobalConstants.CELL_PHYS_SIZE_X,
                        y * GlobalConstants.CELL_PHYS_SIZE_Y
                ));
            }
        }

        while (table.lastKey() < rightBorder){
            LinkedList<Integer> copiedList = table.get(table.lastKey());
            table.put(table.lastKey() + 1,  new LinkedList(copiedList));
            for (int y = copiedList.getFirst(); y <= copiedList.getLast(); y++){
                returnSet.add(new CellKey(
                        table.lastKey() * GlobalConstants.CELL_PHYS_SIZE_X,
                        y * GlobalConstants.CELL_PHYS_SIZE_Y));
            }
        }
    }

    private void findAndDeleteRows(Set<CellKey> returnSet) {
        LinkedList<Integer> columnList = table.get(table.firstKey());
        int downNumber = columnList.getFirst();
        int upNumber = columnList.getLast();

        while(downNumber++ < downBorder){
            for (Map.Entry<Integer, LinkedList<Integer>> entry : table.entrySet()){
                returnSet.add(new CellKey(
                        entry.getKey() * GlobalConstants.CELL_PHYS_SIZE_X,
                        entry.getValue().removeFirst() * GlobalConstants.CELL_PHYS_SIZE_Y)
                );
            }
        }

        while(upNumber-- > upBorder){
            for (Map.Entry<Integer, LinkedList<Integer>> entry : table.entrySet()){
                returnSet.add(new CellKey(
                        entry.getKey() * GlobalConstants.CELL_PHYS_SIZE_X,
                        entry.getValue().removeLast() * GlobalConstants.CELL_PHYS_SIZE_Y)
                );
            }
        }
    }

    private void findAndAddRows(Set<CellKey> returnSet) {

        LinkedList<Integer> columnList = table.get(table.firstKey());
        int downNumber = columnList.getFirst();
        int upNumber = columnList.getLast();

        while(downNumber-- > downBorder){
            for (Map.Entry<Integer, LinkedList<Integer>> entry : table.entrySet()){
                entry.getValue().addFirst(downNumber);
                returnSet.add(new CellKey(
                        entry.getKey() * GlobalConstants.CELL_PHYS_SIZE_X,
                        downNumber * GlobalConstants.CELL_PHYS_SIZE_Y
                ));
            }
        }

        while(upNumber++ < upBorder){
            for (Map.Entry<Integer, LinkedList<Integer>> entry : table.entrySet()){
                entry.getValue().addLast(upNumber);
                returnSet.add(new CellKey(
                        entry.getKey() * GlobalConstants.CELL_PHYS_SIZE_X,
                        upNumber * GlobalConstants.CELL_PHYS_SIZE_Y
                ));
            }
        }
    }
}
