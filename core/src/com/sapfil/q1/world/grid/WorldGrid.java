package com.sapfil.q1.world.grid;

import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.layer.LayerType;
import com.sapfil.q1.state.State;

import java.util.List;
import java.util.Map;

import platform.Disposable;

/**
 * GAME-отображение коллекции ячеек мира, видимых на экране.
 * ВАЖНО! Это не все ячейки мира, а лишь те, что полностью или частично видны на экране.
 * Так же возможно иметь и обрабатывать ячейки, которые полностью не видны на экране, но очень
 * близки к его границе с нешней стороны.
 */
public interface WorldGrid extends Disposable {

    /**
     * @param worldName имя мира - обчыно будет соответствовать имени БД из которой подгружается мир
     * @param state объект, из которого будет доступен view. В этом месте потребуется рефакторинг
     */
    void create(String worldName,
                Map<LayerType, Layer> layerMap,
                State state);

    /**
     * Обновить - пересчитать ячейки мира - какие то догрузить из БД, какие то отгрузить
     * (безопасно удалить из оперативного хранилища, возможно с обновлдением данных в БД).
     */
    void update();

}
