package com.sapfil.q1.world.grid;

import com.badlogic.gdx.Gdx;
import com.sapfil.q1.db.level.key.CellKey;
import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.layer.LayerType;
import com.sapfil.q1.object.GameObjects.GameObject;
import com.sapfil.q1.state.State;

import java.util.*;

import com.sapfil.q1.world.cell.WorldCell;
import com.sapfil.q1.world.db_adapter.WorldDataBaseAdapter;

import platform.Disposable;

public class WorldGridImpl implements WorldGrid {

    private WorldDataBaseAdapter worldDataBaseAdapter;
    private WorldGridResolver worldGridResolver;
    private Layer mainLayer;
    private Layer debugLayer;

    private final Map<CellKey, WorldCell> worldCellMap = new HashMap<>();


    public WorldGridImpl(String worldName,
                         Map<LayerType, Layer> layerMap,
                         State state) {
        create(worldName, layerMap, state);
    }

    @Override
    public void create(String worldName,
                       Map<LayerType, Layer> layerMap,
                       State state) {
        this.worldDataBaseAdapter = new WorldDataBaseAdapter(worldName + ".xml");
        this.worldGridResolver = new WorldGridResolver(state);
        this.mainLayer = layerMap.get(LayerType.MAIN);
        this.debugLayer = layerMap.get(LayerType.DEBUG);

        for (CellKey coordinates : worldGridResolver.getInitSet()){
            worldCellMap.put(coordinates, worldDataBaseAdapter.getWorldCell(
                    coordinates,
                    mainLayer,
                    debugLayer
            ));
        }
    }

    public void adGameObject(GameObject gameObject){
        CellKey cellKey = worldGridResolver.getCellKeyForCoords(gameObject.getAbsX(), gameObject.getAbsY());
        worldCellMap.get(cellKey).addGameObject(gameObject);
    }

    @Override
    public void update() {

        worldGridResolver.update();

        boolean dumpLevelFlag = false;

        for (CellKey deletionCoordinates : worldGridResolver.getDeleteSet()){
            WorldCell removedCell = worldCellMap.remove(deletionCoordinates);
            if (removedCell != null){
                Gdx.app.log("Sapfil_Log", "Removing cell with coords: " + removedCell.getCenterCoordinates());
                if (removedCell.isMutable()) {
                    worldDataBaseAdapter.updateWorldCell(removedCell);
                    dumpLevelFlag = true;
                }
                removedCell.dispose();
            }
        }

        if (dumpLevelFlag){
            worldDataBaseAdapter.dumpLevel();
        }

        for (CellKey coordinates : worldGridResolver.getNewSet()){
            worldCellMap.put(coordinates, worldDataBaseAdapter.getWorldCell(
                    coordinates,
                    mainLayer,
                    debugLayer)
            );
        }
    }

    @Override
    public void dispose() {
        worldCellMap.values().forEach(Disposable::dispose);
    }
}


