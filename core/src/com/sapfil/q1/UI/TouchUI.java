package com.sapfil.q1.UI;

import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.layer.LayerImpl;

/**
 * Created by Sapfil on 07.01.2018.
 */

public class TouchUI extends AUIElement{

        private enum State {STARTING, ON, HIDING, OFF}
        private final static float ICON_RESIZE_SPEED = 4;

        private State state;

        public TouchUI(int id , Layer layer){
           super(id, layer);
           state = State.OFF;
        }

        public void inputTouchCoords(float touchCoordX, float touchCoordY) {
            icon.getLocalCoordinates().setX(touchCoordX);
            icon.getLocalCoordinates().setY(touchCoordY);
            setOn();
        }

        public void setOn(){
            icon.getGfxObject().setEnabled(true);
            icon.getEntity().setScale(0,0);
            state = State.STARTING;
        }

        public void setOff(){
            state = State.HIDING;
        }

        public void update(float dt){
            switch (state){
                case ON:
                case OFF: break;
                case STARTING: {
                    float scale = getIcon().getEntity().getScaleX();
                    if (scale < 1) {
                        if ((scale + ICON_RESIZE_SPEED * dt) < 1)
                            getIcon().getEntity().setScale(scale + ICON_RESIZE_SPEED * dt, scale + ICON_RESIZE_SPEED * dt);
                        else {
                            getIcon().getEntity().setScale(1, 1);
                            state = State.ON;
                        }
                    }
                    break;
                }
                case HIDING:{
                    float scale = getIcon().getEntity().getScaleX();
                    if (scale > 0 ) {
                        if ((scale - ICON_RESIZE_SPEED * dt) > 0)
                            getIcon().getEntity().setScale(scale - ICON_RESIZE_SPEED * dt, scale - ICON_RESIZE_SPEED * dt);
                        else {
                            getIcon().getGfxObject().setEnabled(false);
                            state = State.OFF;
                        }
                    }
                    break;
                }
            }
        }
}
