package com.sapfil.q1.UI;

/**
 * Created by Sapfil on 03.01.2018.
 */

public enum UserCommandType {

    FREE_TOUCH,     // with Vector3 ("unprojected mouse")
    OBJECT_TOUCH,   // with GameObject ("touched object")
    BASE_TOUCH,     // with GameObject ("entrance point")

    B_DEBUG_VISUALISATION_OFF,
    B_DEBUG_VISUALISATION_STATS,
    B_DEBUG_VISUALISATION_PHYS,
    B_DEBUG_SAVE_WORLD,
    B_DEBUG_MOVE_HERO,
    B_DEBUG_DELETE_OBJECTS,
    B_DEBUG_ADD_OBJECTS,
    B_DEBUG_ADD_BORDERS, CHANGE_RENDER_TYPE,

}
