package com.sapfil.q1.UI;

/**
 * Created by Sapfil on 20.01.2018.
 */

public class RegisteredTouchZone {

    private float leftX, upperY, width, height;
    private AUIElement element;

    public RegisteredTouchZone(float leftX, float upperY, float width, float height, AUIElement element) {
        this.leftX = leftX;
        this.upperY = upperY;
        this.width = width;
        this.height = height;
        this.element = element;
    }

    public AUIElement getAUIEifTouched(float x, float y){
        if (x >= leftX)
            if (x <= leftX+width)
                if (y > upperY)
                    if (y <= upperY +height)
                        return element;
        return null;
    }
}
