package com.sapfil.q1.UI;

import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.object.GameObjects.GameObject;

/**
 * Created by Sapfil on 07.01.2018.
 */

public class ButtonUI extends AUIElement {

    public static UI ui;

    protected enum State {ON, OFF}
    protected State state;
    private GameObject subIcon;
    protected UserCommand userCommand;

    public ButtonUI(int id, Layer layer, UserCommand userCommand) {
        super(60007, layer);
        this.userCommand = userCommand;
        icon.getGfxObject().setEnabled(true);
        state = State.OFF;
        if (id > 0) {
            subIcon = new GameObject
                    .GameObjectBuilder()
                    .create(id, layer)
                    .withZcoord(10001)
                    .build();
            if (subIcon.getHealthObject() != null)
                subIcon.getHealthObject().dispose();
        }
    }

    @Override
    public void setOn() {
        icon.getGfxObject().setFrame(0);
        state = State.ON;
    }

    @Override
    public void setOff() {
        icon.getGfxObject().setFrame(1);
        state = State.OFF;
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void setGfxCoords(float x, float y){
        super.setGfxCoords(x, y);
        if (subIcon != null) {
            subIcon.getLocalCoordinates().setX(x);
            subIcon.getLocalCoordinates().setY(y);
        }
    }

    public UserCommand getUserCommand(){
        if (subIcon != null)
            this.userCommand.data = subIcon.getID();
        return this.userCommand;
    }

}
