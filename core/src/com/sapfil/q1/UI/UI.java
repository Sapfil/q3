package com.sapfil.q1.UI;

import com.badlogic.gdx.math.Vector2;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.layer.LayerImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sapfil.q1.GlobalConstants.GFX_TO_PHYS;

/********************************
 * Created by Sapfil
 * Edit 09.05.2017.
 * Notes
 ********************************/

public class UI{

//    private final static float ICON_RESIZE_SPEED = 4;

    private final int  FREE_MOVE_ICON_INDEX;
    private final int  OBJECT_MOVE_ICON_INDEX;
    private final int  BASE_MOVE_ICON_INDEX;

    public enum DebugMode {OFF, PHYSICS, STATS}

    private final Layer layer;
    private DebugUI debugUI;
    private AUIElement currentTouch;
    private AUIElement currentButton = null;
    private List<RegisteredTouchZone> touchZones;

    private List<TouchUI> touchUIS;
    private List<ButtonUI> buttonUIS;
    private Map<AUIElement, Vector2> iconsCoordsCorrections;

    public UI(Layer layer){

        ButtonUI.ui = this;

        this.layer = layer;
        touchUIS = new ArrayList<>();
        buttonUIS = new ArrayList<>();
        touchZones = new ArrayList<>();
        iconsCoordsCorrections = new HashMap<>();
        touchUIS.add(new TouchUI(60010, layer));
        FREE_MOVE_ICON_INDEX = touchUIS.size()-1;
        touchUIS.add(new TouchUI(60011, layer));
        OBJECT_MOVE_ICON_INDEX = touchUIS.size()-1;
        touchUIS.add(new TouchUI(60012, layer));
        BASE_MOVE_ICON_INDEX = touchUIS.size()-1;
        debugUI = new DebugUI();
        currentTouch = touchUIS.get(0);
    }

    // ************* Debug UI part **************
    /** @return true if debug is ON */
    public DebugMode getDebugMode() {return debugUI.getCurrentDebugMode();}
    public void transponCoords(float screenCenterCoordinateX, float screenCenterCoordinateY){
        debugUI.transponCoords(screenCenterCoordinateX, screenCenterCoordinateY);
    }
    public void changeDebugMode(){
        debugUI.changeDebugMode();
    }

    // ************* Touch UI part **************
    public void showFreeSpaceTouch(float touchX, float touchY){
        currentTouch.setOff();
        touchUIS.get(FREE_MOVE_ICON_INDEX).inputTouchCoords(touchX, touchY);
        currentTouch = touchUIS.get(FREE_MOVE_ICON_INDEX);
    }

    public void showObjectTouch(float objectCenterX, float objectCenterY){
        currentTouch.setOff();
        touchUIS.get(OBJECT_MOVE_ICON_INDEX).inputTouchCoords(objectCenterX,objectCenterY);
        currentTouch = touchUIS.get(OBJECT_MOVE_ICON_INDEX);
    }

    public void showBaseTouch(float baseCenterX, float baseCenterY){
        currentTouch.getIcon().getGfxObject().setEnabled(false);
        touchUIS.get(BASE_MOVE_ICON_INDEX).inputTouchCoords(baseCenterX,baseCenterY);
        currentTouch = touchUIS.get(BASE_MOVE_ICON_INDEX);
    }

    public void hideTouches(){
        currentTouch.setOff();
        currentTouch.setOff();
    }

    public void addButton(int[] id, Vector2 gfxCenterCoords, UserCommand userCommand) {
        ButtonUI buttonUI = new MultiButtonUI(id, layer, userCommand);
        registerButton(buttonUI, gfxCenterCoords);
    }

    public void addButton(int id, Vector2 gfxCenterCoords, UserCommand userCommand){
        ButtonUI buttonUI = new ButtonUI(id, layer, userCommand);
        registerButton(buttonUI, gfxCenterCoords);
    }

    private void registerButton (ButtonUI buttonUI, Vector2 gfxCenterCoords){
        buttonUIS.add(buttonUI);
        buttonUI.setOff();
        buttonUI.getIcon().getEntity().setScale(1.0f, 1.0f);
        iconsCoordsCorrections.put(buttonUI,
                new Vector2(
                        gfxCenterCoords.x * GlobalConstants.GFX_TO_PHYS,
                        gfxCenterCoords.y * GlobalConstants.GFX_TO_PHYS)
        );
        touchZones.add(new RegisteredTouchZone(
                gfxCenterCoords.x - 25 + GlobalConstants.WIDTH/2,
                GlobalConstants.HEIGHT - (gfxCenterCoords.y + 25 + GlobalConstants.HEIGHT/2),
                50,
                50,
                buttonUI));
        currentButton = buttonUI;
    }

    public UserCommand resolveTouch(float x, float y){
        for (RegisteredTouchZone rtz :touchZones){
            AUIElement auie = rtz.getAUIEifTouched(x, y);
            if (auie != null) {
                if (!auie.equals(currentButton))
                    currentButton.setOff();
                auie.setOn();
                currentButton = auie;
                return ((ButtonUI)currentButton).getUserCommand();
            }
        }
        return null;
    }

    public void update(float dt){

        for (TouchUI icon : touchUIS)
            icon.update(dt);
    }

    private class DebugUI {

        private DebugMode currentDebugMode;

        public DebugMode getCurrentDebugMode() {
            return currentDebugMode;
        }

        private float screenCorrectionX;
        private float screenCorrectionY;

        DebugUI(){
            currentDebugMode = GlobalConstants.DEBUG_MODE;
            for (AUIElement buttonUI : buttonUIS){
                screenCorrectionX = -GlobalConstants.WIDTH/2 + buttonUI.getIcon().getGfxObject().getGfxCenterX();
                screenCorrectionY = GlobalConstants.HEIGHT/2 - buttonUI.getIcon().getGfxObject().getGfxCenterY();
                screenCorrectionX *= GFX_TO_PHYS;
                screenCorrectionY *= GFX_TO_PHYS;
                iconsCoordsCorrections.put(buttonUI, new Vector2(screenCorrectionX, screenCorrectionY));
            }
        }

        public void transponCoords(float screenCenterCoordinateX, float screenCenterCoordinateY){
            for (AUIElement aui : buttonUIS)
                aui.setGfxCoords(
                        screenCenterCoordinateX + iconsCoordsCorrections.get(aui).x,
                        screenCenterCoordinateY + iconsCoordsCorrections.get(aui).y);
        }

        public void changeDebugMode(){
            switch (currentDebugMode){
                case OFF: {
                    currentDebugMode = DebugMode.STATS; break;}
                case STATS: {
                    currentDebugMode = DebugMode.PHYSICS; break;}
                case PHYSICS: {
                    currentDebugMode = DebugMode.OFF; break;}
                default: currentDebugMode = DebugMode.OFF;
            }
        }

    }
}