package com.sapfil.q1.UI;

/**
 * Created by Sapfil on 03.01.2018.
 */

public class UserCommand {

    public UserCommandType type;
    public Object data;

    public UserCommand(UserCommandType type) {
        this.type = type;
    }

    public UserCommand(UserCommandType type, Object data) {
        this.type = type;
        this.data = data;
    }
}
