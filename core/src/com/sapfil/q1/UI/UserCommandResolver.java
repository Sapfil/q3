package com.sapfil.q1.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.sapfil.q1.GlobalConstants;
import com.sapfil.q1.object.PhysicsObjects.HealthObjectCollection;
import com.sapfil.q1.object.PhysicsObjects.IHealthObject;

import static com.sapfil.q1.UI.UserCommandType.BASE_TOUCH;
import static com.sapfil.q1.UI.UserCommandType.OBJECT_TOUCH;

/**
 * Created by Sapfil on 03.01.2018.
 */

public class UserCommandResolver {
   public static UserCommand resolveEditorTouch(Vector3 mouse,
                                                OrthographicCamera camera,
                                                HealthObjectCollection objectCollection,
                                                boolean processContiniousTouch){

       if (Gdx.input.justTouched()) {
           if (!processContiniousTouch){
               return getUserCommand(mouse, camera, objectCollection);
           }
       }

       if (processContiniousTouch)
           if (Gdx.input.isTouched()) {
           if (mouse.x > 50 && mouse.x < GlobalConstants.WIDTH - 50) {
               return getUserCommand(mouse, camera, objectCollection);
           }
       }
       return null;
   }

    private static UserCommand getUserCommand(Vector3 mouse, OrthographicCamera camera, HealthObjectCollection objectCollection) {
        camera.unproject(mouse);

        IHealthObject touchedObject = objectCollection.getObjectContsinsPoint(mouse.x, mouse.y);
        if (touchedObject != null) {
            switch (touchedObject.getMotherObject().getTypeOfObject()) {
                case 100:
                    return new UserCommand(OBJECT_TOUCH, touchedObject.getMotherObject());
                case 800:
                    return new UserCommand(BASE_TOUCH, touchedObject.getMotherObject().getBaseProperty().getEntrancepoint());
            }
        }

        Vector3 touchInGrid = new Vector3();
        touchInGrid.x = MathUtils.round(mouse.x / (GlobalConstants.WORLD_GRID_STEP_X * GlobalConstants.GFX_TO_PHYS));
        if (touchInGrid.x % 2 != 0)
            mouse.y -= (GlobalConstants.WORLD_GRID_STEP_Y * GlobalConstants.GFX_TO_PHYS);
        touchInGrid.y = MathUtils.round(mouse.y / (GlobalConstants.WORLD_GRID_STEP_Y * 2 * GlobalConstants.GFX_TO_PHYS));
         return new UserCommand(UserCommandType.FREE_TOUCH, touchInGrid);
    }
}
