package com.sapfil.q1.UI;

import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.object.GameObjects.GameObject;

/**
 * Created by Sapfil on 07.01.2018.
 */

public abstract class AUIElement {

    public GameObject getIcon() {
        return icon;
    }

    protected GameObject icon;

    AUIElement(int id , Layer layer){
        icon = new GameObject
                .GameObjectBuilder()
                .create(id, layer)
                .withZcoord(10000)
                .build();

        icon.getGfxObject().setEnabled(false);
    }

    public abstract void setOn();
    public abstract void setOff();
    public abstract void update(float dt);

    public void setGfxCoords(float x, float y){
        icon.getLocalCoordinates().setX(x);
        icon.getLocalCoordinates().setY(y);
    }
}
