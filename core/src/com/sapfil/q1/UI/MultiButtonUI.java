package com.sapfil.q1.UI;

import com.sapfil.q1.layer.Layer;
import com.sapfil.q1.object.GameObjects.GameObject;

/**
 * Created by Sapfil on 07.01.2018.
 */

public class MultiButtonUI extends ButtonUI {

    private GameObject[] subIcons;
    private int currentIconNumber = 0;

    public MultiButtonUI(int[] id, Layer layer, UserCommand userCommand) {
        super(0, layer, userCommand);
        icon.getGfxObject().setEnabled(true);
        state = State.OFF;
        subIcons = new GameObject[id.length];
        for (int i = 0; i < id.length; i++){
            subIcons[i] = new GameObject
                    .GameObjectBuilder()
                    .create(id[i], layer)
                    .withZcoord(10001)
                    .build();
            subIcons[i].setVisible(false);
            if (subIcons[i].getGfxObject().getRegionHeight() > icon.getGfxObject().getRegionHeight()*0.8f ||
                subIcons[i].getGfxObject().getRegionWidth() > icon.getGfxObject().getRegionWidth()*0.8f) {
                float scaleX = icon.getGfxObject().getRegionWidth() * 0.8f / subIcons[i].getGfxObject().getRegionWidth();
                float scaleY = icon.getGfxObject().getRegionHeight() * 0.8f / subIcons[i].getGfxObject().getRegionHeight();
                float scale = scaleX < scaleY ? scaleX : scaleY;
                subIcons[i].getEntity().setScale(scale, scale);
            }
            if (subIcons[i].getHealthObject()!= null)
                subIcons[i].getHealthObject().dispose();
        }
        subIcons[0].setVisible(true);
    }

    @Override
    public void setOn() {

        if (state == State.ON) {
            subIcons[currentIconNumber].setVisible(false);
            currentIconNumber += 1;
            if (currentIconNumber == subIcons.length)
                currentIconNumber = 0;
            subIcons[currentIconNumber].setVisible(true);
        }

        icon.getGfxObject().setFrame(0);
        state = State.ON;
    }

//    @Override
//    public void setOff() {
//        icon.getGfxObject().setFrame(1);
//        state = State.OFF;
//}

//    @Override
//    public void update(float dt) {
//
//    }

    @Override
    public void setGfxCoords(float x, float y){
        super.setGfxCoords(x, y);
        for (GameObject subIcon : subIcons) {
            subIcon.getLocalCoordinates().setX(x);
            subIcon.getLocalCoordinates().setY(y);
        }
    }

    @Override
    public UserCommand getUserCommand(){
        this.userCommand.data = subIcons[currentIconNumber].getID();
        return this.userCommand;
    }

}
