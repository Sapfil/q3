package com.sapfil.q1.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sapfil.q1.Editor;

public class DesktopEditorLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width 	= 800;
		config.height	= 480;
		config.title	= "Editor";

		config.x = 100;
				config.y = 100;

				new LwjglApplication(new Editor(), config);
				}
				}
